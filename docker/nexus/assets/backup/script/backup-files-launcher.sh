#!/bin/bash
APP_NAME=nexus
APP_HOME=/opt/nexus
BKP_FILES_FOLDER=${BKP_FOLDER}/backups/${APP_NAME}/files
export MAX_BACKUPS=10

echo BKP_FILES_FOLDER: ${BKP_FILES_FOLDER}

if [ ! -d "${BKP_FILES_FOLDER}" ]; then
 echo making dir: ${BKP_FILES_FOLDER}
 mkdir -p ${BKP_FILES_FOLDER}
fi

${BKP_FOLDER}/${APP_NAME}-bkp/scripts/backup-files.sh ${APP_HOME} ${BKP_FILES_FOLDER}/${APP_NAME}-bkp-$(date +%Y%m%d.%H%M%S).tar.gz
