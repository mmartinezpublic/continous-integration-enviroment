#!/bin/bash
set -e
trap "break;exit" SIGHUP SIGINT SIGTERM
	
	APPLICATION_INIT_FOLDER=/usr/share/jenkins/init
	DOCKER_ENTRYPOINT="/usr/local/bin/jenkins.sh"
	TEMP_JENKINS="/tmp/jenkins"

if [ ! -f ${APPLICATION_INIT_FOLDER}/DONE ]; then   
    echo "RELOAD CONFIG"       
    pwd
	cp -r ${TEMP_JENKINS}/* ${JENKINS_HOME}
	cp ${MAVEN_HOME}/conf/settings.xml ${MAVEN_CONF_PATH}
    echo "SUCCESS" >> ${APPLICATION_INIT_FOLDER}/DONE
else
    echo "NOT RELOAD CONFIG"
fi

echo "STARTIN APPLICATION"
${APPLICATION_INIT_FOLDER}/wait-for-it.sh --host=sonar --port=9000 --timeout=180
${DOCKER_ENTRYPOINT}