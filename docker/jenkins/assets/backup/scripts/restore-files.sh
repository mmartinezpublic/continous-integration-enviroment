#!/bin/bash

##################################################################################
function usage(){
  echo "usage: $(basename $0) /path/to/app_home archive.tar.gz"
}
##################################################################################

readonly APP_HOME=$1
readonly BKP_PATH=$2
readonly CUR_DIR=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)

if [ -z "${APP_HOME}" -o -z "${BKP_PATH}" ] ; then
  usage >&2
  exit 1
fi

readonly RESTORE_BKP_TMP_FOLDER=${APP_HOME}/files_previous_bkp$(date +%Y%m%d.%H%M%S)
mkdir -p ${RESTORE_BKP_TMP_FOLDER}
echo RESTORE_BKP_TMP_FOLDER: ${RESTORE_BKP_TMP_FOLDER}
mv -vf "${APP_HOME}/"*.xml "${RESTORE_BKP_TMP_FOLDER}"
mkdir "${RESTORE_BKP_TMP_FOLDER}/plugins"
mkdir "${RESTORE_BKP_TMP_FOLDER}/users"
mkdir "${RESTORE_BKP_TMP_FOLDER}/secrets"
mkdir "${RESTORE_BKP_TMP_FOLDER}/nodes"
mkdir "${RESTORE_BKP_TMP_FOLDER}/scriptler"
mkdir "${RESTORE_BKP_TMP_FOLDER}/.m2"

mv -vf ${APP_HOME}/plugins/* "${RESTORE_BKP_TMP_FOLDER}/plugins"
mv -vf ${APP_HOME}/users/* "${RESTORE_BKP_TMP_FOLDER}/users"
mv -vf ${APP_HOME}/secrets/* "${RESTORE_BKP_TMP_FOLDER}/secrets"
mv -vf ${APP_HOME}/nodes/* "${RESTORE_BKP_TMP_FOLDER}/nodes"
mv -vf ${APP_HOME}/scriptler/* "${RESTORE_BKP_TMP_FOLDER}/scriptler"
mv -vf ${APP_HOME}/.m2/* "${RESTORE_BKP_TMP_FOLDER}/.m2"
mv -vf ${APP_HOME}/*identity* "${RESTORE_BKP_TMP_FOLDER}"
mv -vf ${APP_HOME}/*secret.key* "${RESTORE_BKP_TMP_FOLDER}"

tar zxvf "${BKP_PATH}" -C "${APP_HOME}" --strip-components=1

echo "=> Restore done"

exit 0