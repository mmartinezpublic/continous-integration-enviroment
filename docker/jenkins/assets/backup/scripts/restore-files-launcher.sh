#!/bin/bash
APP_NAME=jenkins
APP_HOME=/opt/jenkins
export BKP_APP_FOLDER=/opt/backup/${APP_NAME}-bkp
export BKP_FILES_FOLDER=${BKP_APP_FOLDER}/backups/${APP_NAME}/files

ACTIVE=$(sudo service ${APP_NAME} status | grep Active:)
if [ ! -z "${ACTIVE}" ]; then
 echo "The service is up. Please stop service"
 echo ${ACTIVE}
fi

if [ ! -d "${BKP_APP_FOLDER}" ]; then
 echo "Restore folder doesn't exist:" ${BKP_FILES_FOLDER}
fi

if [ -d "${BKP_APP_FOLDER}" ]; then
 BKP_PATH=$(ls -r ${BKP_FILES_FOLDER}/${APP_NAME}-bkp*.tar.gz | head -n 1)
 echo TO BKP_PATH: ${BKP_PATH}
 echo TO APP_HOME: ${APP_HOME}
 ${BKP_APP_FOLDER}/scripts/restore-files.sh ${APP_HOME} ${BKP_PATH}
 sudo chown -R jenkins:jenkins ${APP_HOME}
fi