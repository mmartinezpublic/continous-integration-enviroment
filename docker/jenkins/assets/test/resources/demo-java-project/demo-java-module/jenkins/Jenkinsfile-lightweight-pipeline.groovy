@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this,'ligthweight')

/* Project 4 letters. */ 
def project='BMDL'

/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)
         checkout scm
         utils.prepare()
      }
      stage('Build') {
         utils.buildMaven()
      }
      stage('Results') {
         utils.saveResult('jar')
      }
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }

   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}