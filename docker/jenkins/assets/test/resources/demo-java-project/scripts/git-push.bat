@ECHO ON
cls

ECHO. [%time%] - GIT INITIAL PUSH
CD %~dp0
SET CURRENT_APP_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %~dp0
%~d0

REM CALL D:\app-dev\configure-env.bat

SET ORIGIN_NAME=demo-java-project
SET ORIGIN_GIT_URL=http://prmcclnxd01:8084/shared/demo-java-project.git

cd /D %CURRENT_APP_DIR%
cd ..
echo CD: %CD%
PAUSE
REM set proxy=http://localhost:3128
REM SET HTTP_PROXY=%proxy%
REM SET HTTPS_PROXY=%proxy%
REM SET http_proxy=%proxy%
REM SET https_proxy=%proxy%
git config --global http.sslVerify false
git remote remove %ORIGIN_NAME%

echo CD: %CD%
git init
git remote add %ORIGIN_NAME% %ORIGIN_GIT_URL%
git add .
git commit -m "commit %ORIGIN_NAME%"
git push -u %ORIGIN_NAME% master

Pause