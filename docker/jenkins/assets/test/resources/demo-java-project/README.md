# README #

### What is this repository for? ###

* Demo java project is an example of java project template, that is easy to integrato to CI/CD enviroment

* Version: 1.0 - Initial version

### How do I get set up? ###

* Summary of set up: 
Clone the repository and personalize the project with your needs

* Configuration: 
Change POM with the project names

* Dependencies: 
None

* Database configuration: 
None

* How to run tests: 
Create the job and run the test from CI enviroment

* Deployment instructions: 
Use CD enviroment

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

- support.devops.bcp@everis.com
- miguel.angel.martinez.espichan@everis.com
- edith.erika.puclla.pareja@everis.com