/**
 * 
 */
package com.base.enviroment.test;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author Miguel
 *
 */
public class NexusHelloTest {

	/**
	 * Test method for {@link com.base.enviroment.test.NexusHello#getHello(java.lang.String)}.
	 */
	@Test
	public void testGetHello() {
		NexusHello nexusHello=new NexusHello();
		Assert.assertEquals("Error on test Hello","Hello from Nexus Lib Jenkins", nexusHello.getHello("Jenkins"));
	}

}
