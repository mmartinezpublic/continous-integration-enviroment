@ECHO ON
cls

ECHO. [%time%] - GIT INITIAL PUSH
CD %~dp0
SET CURRENT_APP_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %~dp0
%~d0

REM CALL D:\app-dev\configure-env.bat

cd /D %CURRENT_APP_DIR%
cd ..
echo CD: %CD%
PAUSE
set proxy=http://localhost:3128
SET HTTP_PROXY=%proxy%
SET HTTPS_PROXY=%proxy%
SET http_proxy=%proxy%
SET https_proxy=%proxy%
git config --global http.sslVerify false
REM git remote remove origin

echo CD: %CD%
git init
git remote add origin http://prmcclnxd01:8084/bmdl/bitwiseman-shared.git
git add .
git commit -m "commit origin"
git push -u origin master

Pause