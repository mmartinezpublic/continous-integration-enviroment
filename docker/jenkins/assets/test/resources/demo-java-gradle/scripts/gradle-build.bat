@ECHO OFF
cls

ECHO. [%time%] - EXECUTE GRADLE BUILD
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CALL D:\app-dev\configure-env.bat
CD /D %CURRENT_DIR%
cd..

CALL gradle build -debug

PAUSE