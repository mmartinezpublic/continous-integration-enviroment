@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this,'daily')

/* Project 4 letters. */ 
def project='BMDL'

/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients

def dockerRegistry='http://localhost:5000'
def dockerServer='tcp://localhost:4243'
def dockerImageTag='bmdl/generate-ssh-credentials'
def tagVersion='1.0'

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)   
         checkout scm      
         utils.prepare()
         //FOR SECURITY THE JOB DOESN'T STORE 1 LOG HISTORY
         def buildDiscarderProperty = [$class: 'BuildDiscarderProperty',strategy: [$class: 'LogRotator', numToKeepStr: '1']]
         def properties = [buildDiscarderProperty]
         this.properties(
             properties
         )
      }
          
	  def credentialContainer = null	  
	  
	  stage('Generate Image if not exits ') {
	  	existTag = sh(
	        returnStdout: true,
	        script: """
	        function docker_tag_exists() { 
	        	curl --silent -f -lSL ${dockerRegistry}/v1/repositories/${dockerImageTag}/tags/${tagVersion} > /dev/null 
	        } 
	        if docker_tag_exists; 
	        	then echo exist 
	        else 
	        	echo not exists 
	        fi
			"""
        ).trim()
        echo 'EXISTS TAG: '+existTag
        if(existTag!='exist'){
		 	docker.withRegistry(dockerRegistry) {
		  		docker.withServer(dockerServer){	  		
		  			credentialContainer = docker.build(dockerImageTag,'./docker/generate-credentials-docker')
		  			credentialContainer.push(tagVersion)
				}
		  	}
	  	}
	  }
	
	  stage('Generate key') {
		  docker.withRegistry(dockerRegistry) {
		  	docker.withServer(dockerServer){
		  		 def sshKeys=sh(script: 'docker run bmdl/generate-ssh-credentials:1.0', returnStdout: true)
		  		 echo "SSHKEYS:\n"+ sshKeys
			 }
	      }
	  }
	  
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }
   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}