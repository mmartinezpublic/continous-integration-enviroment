@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this,'daily')

/* Project 4 letters. */ 
def project='BMDL'

/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients

def dockerRegistry='http://localhost:5000'
def dockerServer='tcp://localhost:4243'
def dockerSshToolsImageTag='ebiwd/alpine-ssh:3.6'
def tagVersion='1.0'

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)   
         checkout scm      
         utils.prepare()                                                                                                                                                      
         //FOR SECURITY THE JOB DOESN'T STORE 1 LOG HISTORY
         //def buildDiscarderProperty = [$class: 'BuildDiscarderProperty',strategy: [$class: 'LogRotator', numToKeepStr: '1']]
         //def properties = [buildDiscarderProperty]
         //this.properties(
         //    properties
         //)
      }
	  
      milestone(label:'Before Copy SSH Public Keys to Server')
      stage('Copy SSH Public Keys to Server') {
      	 def targetServers;
      	 def targetServersList=null;
         if(!env.REMOTE_USER){
         	error("REMOTE_USER must not be set")         	
         }
         if(!env.REMOTE_PASSWORD){
         	error("REMOTE_PASSWORD must not be set")         	
         }
         if(!env.PUBLIC_SSH_KEY){
         	error("PUBLIC_SSH_KEY must not be set")         	
         }
         if(!env.TARGET_SERVER){
         	error("REMOTE_USER must not be set")
         }else{
         	targetServers=env.TARGET_SERVER
         	echo "targetServers: "+targetServers
         	targetServersList=targetServers.split("\\n")
         	echo "targetServersList: "+targetServersList
         }
      	 //Start ssh agent to deploy solution
      	 milestone(label:'Copping SSH Public Keys to Server')
      	 node {
			 echo 'DEPLOYMENT SERVER: '+ env.TARGET_SERVER
			 echo 'REMOTE USER: '+ env.REMOTE_USER
			 ws{
			 	wrap([$class: 'MaskPasswordsBuildWrapper', varPasswordPairs: [[password: '${PUBLIC_SSH_KEY}', var: 'PUBLIC_SSH_KEY_ENV']]]) {
				 	sh ( script:"echo ${PUBLIC_SSH_KEY} >> ${WORKSPACE}/ssh-public-key-jks-${BUILD_NUMBER}-id_rsa.pub", returnStdout: false)
					int serverListSize=targetServersList.size()
					for ( int i = 0; i < serverListSize; i++ ) {
					   String targetServerItem=targetServersList[i]
					   echo 'Executing command on : ****'+targetServerItem+'****'
					   def userRemotePath=sh ( script:"sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${targetServerItem} sh -c \'cd && mkdir -p ~${REMOTE_USER}/.ssh\'", returnStdout: true)
					   echo "USER REMOTE PATH:\n"+ userRemotePath
					   userRemotePath= sh ( script:"sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh ${REMOTE_USER}@${targetServerItem} sh -c \'cd && ls -al ~${REMOTE_USER}/.ssh\'", returnStdout: true)
					   sh ( script:"sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${targetServerItem} sh -c \'cat >> ~${REMOTE_USER}/.ssh/authorized_keys\' < ${WORKSPACE}/ssh-public-key-jks-${BUILD_NUMBER}-id_rsa.pub", returnStdout: true)
					   echo "USER REMOTE PATH:\n"+ userRemotePath
					   sh "sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${targetServerItem} \"chmod 700 ~${REMOTE_USER}/.ssh\""
					   sh "sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${targetServerItem} \"chmod 644 ~${REMOTE_USER}/.ssh/authorized_keys\""
					   userRemotePath= sh ( script:"sshpass -p \""+env.REMOTE_PASSWORD+"\" ssh -o StrictHostKeyChecking=no ${REMOTE_USER}@${targetServerItem} sh -c \'cd && ls -al ~${REMOTE_USER}/.ssh\'", returnStdout: true)
					   echo "USER REMOTE PATH:\n"+ userRemotePath
				    }
				}
			 }
		 }
	  }
	  milestone(label:'SSH Public Keys are copied')
	  
		  
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }
   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}