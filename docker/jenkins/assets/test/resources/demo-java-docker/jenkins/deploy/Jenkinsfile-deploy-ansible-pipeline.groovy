@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this)

/* Set the path for configuration properties, from which this file will resolve variables */
def configPath="jenkins/config.properties"

/* Project settings */
def project=""

/* Docker Settings. */
def dockerHost=""
def dockerRegistryURL=""
def dockerRunArgs=""
def buildTag=""
def appName=""
def imageName=""
def containerName=""

/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients=""

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)
         dockerHost="${env.DOCKER_HOST}"
         dockerRegistryURL="${env.DOCKER_REGISTRY_URL}"
         checkout scm
         utils.prepare()
         //Setup parameters
         def jobCustomProperties = readProperties file: "${configPath}"
         project=jobCustomProperties['project']	
		 recipients=jobCustomProperties['mail.recipients']	
		 appName=jobCustomProperties['app.name']
		 buildTag=jobCustomProperties['docker.buildTag']
		 dockerRunArgs=jobCustomProperties['docker.run.args']
		 containerName="some"+appName
		 imageName=project.toLowerCase()+"/"+appName+":"+buildTag
      }
      stage('Build') {
         utils.buildMaven()
      }
      stage('Results') {
         utils.saveResult('jar')
      }
      stage('Sonar') {
         utils.executeSonar()
      }
      stage('Nexus') {
         utils.deployNexusArtifact(project)
      }
      stage('Docker Build') {
         echo 'Image name: '+ imageName
         utils.builSprintBootDockerFile(dockerHost,imageName)
      }
      stage('Docker Push') {
         echo 'Image name: '+ imageName
         echo 'Registry URL: '+ dockerRegistryURL
         utils.pushDockerRegistry(dockerHost,dockerRegistryURL,imageName)
      }
      
	  milestone(label:'Prepare for release to DEV')
      stage('Release to DEV') {
      	 //Start ssh agent to deploy solution
      	 def deploymentEnviroment="dev"
      	 def jobCustomProperties = readProperties file: "${configPath}"
		 def remoteUser=jobCustomProperties["deploy."+deploymentEnviroment+".user"]		 
		 def credentialsId=project.toLowerCase()+'-jenkins-ssh-privatekey-'+deploymentEnviroment
      	 milestone(label:'Releasing to DEV')
		 ansiblePlaybook( 
                playbook: "ansible/site.yaml",
                inventory: "ansible/hosts",
                credentialsId: "${credentialsId}",
                extraVars: [ 
                        ansible_connection: "ssh",
                        workdir: "ansible",
                        ansible_user: "${remoteUser}",
                        extras: "-vv",
                        dir_lib: "roles/test-copy/defaults"
                        ]
         ) 
	  }
	  milestone(label:'Released to DEV')
	  
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }
   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}