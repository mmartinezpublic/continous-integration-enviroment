@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this)

/* Set the path for configuration properties, from which this file will resolve variables */
def configPath="jenkins/config.properties"

/* Project settings */
def project=""

/* Docker Settings. */
def dockerHost=""
def dockerRegistryURL=""
def dockerRunArgs=""
def buildTag=""
def appName=""
def imageName=""
def containerName=""

/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients=""

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)
         dockerHost="${env.DOCKER_HOST}"
         dockerRegistryURL="${env.DOCKER_REGISTRY_URL}"
         checkout scm
         utils.prepare()
         //Setup parameters
         def jobCustomProperties = readProperties file: "${configPath}"
         project=jobCustomProperties['project']	
		 recipients=jobCustomProperties['mail.recipients']	
		 appName=jobCustomProperties['app.name']
		 buildTag=jobCustomProperties['docker.buildTag']
		 dockerRunArgs=jobCustomProperties['docker.run.args']
		 containerName="some"+appName
		 imageName=project.toLowerCase()+"/"+appName+":"+buildTag	 
      }
      stage('Build') {
         //utils.buildMaven()
      }
      stage('Results') {
         //utils.saveResult('jar')
      }
      stage('Sonar') {
         //utils.executeSonar()
      }
      stage('Nexus') {
         //utils.deployNexusArtifact(project)
      }
      stage('Docker Build') {
         echo 'Image name: '+ imageName
         //utils.builSprintBootDockerFile(dockerHost,imageName)
      }
      stage('Docker Push') {
         echo 'Image name: '+ imageName
         echo 'Registry URL: '+ dockerRegistryURL
         //utils.pushDockerRegistry(dockerHost,dockerRegistryURL,imageName)
      }
      
	  milestone(label:'Prepare for release to DEV')
      stage('Release to DEV') {
      	 //Start ssh agent to deploy solution
      	 milestone(label:'Releasing to DEV')
      	 //Variable for deployment
      	 def jobCustomProperties = readProperties file: "${configPath}"
      	 def deploymentEnviroment="dev"
		 def targetServer=jobCustomProperties["deploy."+deploymentEnviroment+".targetServer"]
		 def remoteUser=jobCustomProperties["deploy."+deploymentEnviroment+".user"]
		 def credentialsId=project.toLowerCase()+'-jenkins-ssh-privatekey-'+deploymentEnviroment
         sshagent(credentials: [credentialsId]) {
			 echo 'DEPLOY ENVIROMENT: '+deploymentEnviroment
			 echo 'DEPLOYMENT SERVER: '+targetServer
			 echo 'REMOTE USER: '+remoteUser
			 echo 'USING CREDENTIAS: '+credentialsId
			 sh "ssh -vvv -o StrictHostKeyChecking=no ${remoteUser}@${targetServer} echo 'test connection'"
			 sh "ssh -o StrictHostKeyChecking=no ${remoteUser}@${targetServer} dockerRunArgs=\'${dockerRunArgs}\' registryHost=${dockerRegistryURL} imageName=${imageName} 'bash -s' < ./scripts/deploy/application-deploy.sh"
		 }
	  }
	  milestone(label:'Released to DEV')
	  
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }
   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}