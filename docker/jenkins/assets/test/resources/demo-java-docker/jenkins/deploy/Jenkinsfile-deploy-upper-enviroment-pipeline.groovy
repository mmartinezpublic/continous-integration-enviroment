@Library('jenkins-sharedlib@master')
import sharedlib.JenkinsfileUtil
def utils = new JenkinsfileUtil(steps,this)

/* Set the path for configuration properties, from which this file will resolve variables */
def configPath="jenkins/config.properties"

/* Project settings */
def project=""

/* Docker Settings. */
def dockerRegistryURL=""
def buildTag=""
def myapp=""
def imageName=""

/* Deploy settings */
def deploymentEnviroment=""
def targetServer=""
def remoteUser=""
def credentialsId=""
 
/* Mail configuration*/
// If recipients is null the mail is sent to the person who start the job
// The mails should be separated by commas(',')
def recipients=""

try {
   node {
      stage('Preparation') {
         utils.notifyByMail('START',recipients)
         dockerHost="${env.DOCKER_HOST}"
         dockerRegistryURL="${env.DOCKER_REGISTRY_URL}"
         checkout scm
         utils.prepare()
         //Setup parameters
         deploymentEnviroment=env.DEPLOY_ENVIROMENT.toLowerCase()
         def jobCustomProperties = readProperties file: "${configPath}"
         project=jobCustomProperties['project']	
		 targetServer=jobCustomProperties['deploy.'+deploymentEnviroment+'.targetServer']
		 remoteUser=env.REMOTE_USER
		 credentialsId=project.toLowerCase()+'-jenkins-ssh-privatekey-'+deploymentEnviroment
		 recipients=jobCustomProperties['mail.recipients']	
		 buildTag=jobCustomProperties['docker.buildTag']
		 myapp=jobCustomProperties['app.name']
		 imageName=project.toLowerCase()+"/"+myapp+":"+buildTag	 
		 
      }
      milestone(label:'Promote Nexus: '+deploymentEnviroment.toUpperCase())
      stage('Promote Nexus Pre-release') {
         echo "PARENT JOBNAME: ${env.PARENT_BUILD_NUMBER_JOBNAME}"
         echo "PARENT JOBNUMBER: ${env.PARENT_BUILD_NUMBER_NUMBER}"
         milestone(label:'Deploying to Nexus: '+deploymentEnviroment.toUpperCase())
      	 node {	
      	   //utils.deployNexusArtifact(project)
         }
      }
      milestone(label:'Deployed to Nexus: '+deploymentEnviroment.toUpperCase())
      
      milestone(label:'Prepare for release to '+deploymentEnviroment.toUpperCase())
      stage('Release to '+deploymentEnviroment.toUpperCase()) {
      	 //Start ssh agent to deploy solution
      	 milestone(label:'Releasing to '+deploymentEnviroment.toUpperCase())
      	 node {
			 sshagent(credentials: [credentialsId]) {
			      echo 'DEPLOY ENVIROMENT: '+deploymentEnviroment
			      echo 'DEPLOYMENT SERVER: '+targetServer
			      echo 'REMOTE USER: '+remoteUser
			      echo 'USING CREDENTIAS: '+credentialsId
			 	  sh "ssh -vvv -o StrictHostKeyChecking=no ${remoteUser}@${targetServer} echo 'test connection'"
			      sh "ssh -o StrictHostKeyChecking=no ${remoteUser}@${targetServer} registryHost=${dockerRegistryURL} imageName=${imageName} 'bash -s' < ./scripts/deploy/application-deploy.sh"
			 }
		 }
	  }
	  milestone(label:'Released to '+deploymentEnviroment.toUpperCase())
	  
      stage('Post Execution') {
         utils.executePostExecutionTasks()
         utils.notifyByMail('SUCCESS',recipients)
      }
   }
} catch(Exception e) {
   node{
      utils.executeOnErrorExecutionTasks()
      utils.notifyByMail('FAIL',recipients)
    throw e
   }
}