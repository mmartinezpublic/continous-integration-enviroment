 package sharedlib;
/***
* 
* JenkinsfileUtil - version 1.1 
*
* Changelog:
* - 1.0 Initial implementation 
* - 1.1 Add NodeJS support
* - 1.2 Add Support for docker registry
* 
*/
import org.jenkinsci.plugins.docker.workflow.*;

class JenkinsfileUtil implements Serializable {
  /* General Properties */
  def sonarqubeUrl
  def branchName
  def mvnHome
  def gradleHome
  def nodeHome
  def mavenTool="MAVEN_3.3.9"
  def gradleTool="GRADLE_2.14.1"
  def nodeJSTool="NODEJS_6.9.2"
  def repositoryId
  def container_name
  def newApp
  
  /*
  * Get information from the main pipeline
  */
  def steps
  def script
  def docker
  def type
  JenkinsfileUtil(steps,script,String type = '') {
      this.steps = steps
      this.script = script
      steps.echo "steps: ${steps}"
      steps.echo "script: ${script}"
      docker=new Docker(this.script)
      this.type=type
  }
  
  def getProjectVersion(){
    def file = steps.readFile('pom.xml')
    def project = new XmlSlurper().parseText(file)
    return project.version.text()
  }

  /*
  * This method checkout the code from git and configure settings for the project
  */
  def prepare() {
     sonarqubeUrl=script.env.SONARQUBE_URL
     
     if(type!=null&&!type.empty&&type=='DISABLEDligthweight'){
       def gitLabTrigger = [
          $class: "GitLabPushTrigger",
          triggerOnPush: true,
          triggerOnMergeRequest: true,
          triggerOnNoteRequest: true,
          acceptMergeRequestOnSuccess: true,
          branchFilterType: "All",
          ciSkip: true, // this is the relevant bit
          triggerOpenMergeRequestOnPush: "never",
          setBuildDescription: true,
          skipWorkInProgressMergeRequest: true,
          noteRegex: "Jenkins please retry a build"
       ]
       def buildDiscarderProperty = [$class: 'BuildDiscarderProperty',strategy: [$class: 'LogRotator', numToKeepStr: '4']]
       def properties = [buildDiscarderProperty]
       properties.add(steps.pipelineTriggers([gitLabTrigger]))
       /* Only keep the 4 most recent builds - To improve performance of the job */
       steps.properties(
           properties
       )
       steps.quietPeriod(10)
     }
    
     // Get the Maven tool.
     // ** NOTE: This 'M3' Maven tool must be configured
     // **       in the global configuration.           
     mvnHome = steps.tool "${mavenTool}"
     gradleHome = steps.tool "${gradleTool}"
     nodeHome = steps.tool "${nodeJSTool}"
     steps.echo "GRADLE_HOME:${gradleHome}"
     steps.echo "MAVEN_HOME: ${mvnHome}"
     steps.echo "NODE_HOME: ${nodeHome}"
     branchName = "${script.env.gitlabSourceBranch}"
     steps.echo "************************************************" 
     if(type=='ligthweight'){
        steps.echo "USER:  ${script.env.gitlabUserName}" 
        steps.echo "USERMAIL:  ${script.env.gitlabUserEmail}" 
     }else{
     	branchName = steps.sh (
       		script: 'git name-rev --name-only HEAD | sed "s?.*remotes/origin/??"',
       		returnStdout: true
     	).trim()
     }
     steps.echo "BRANCH_NAME: ${branchName}"
  }
  /*
  * This method compile the doce from maven
  */
  def buildMaven() {
     // Run the maven build
     if (steps.isUnix()) {
        steps.sh "'${mvnHome}/bin/mvn' clean package"
     } else {
        steps.bat(/"${mvnHome}\bin\mvn" clean package/)
     }
     try {
         steps.step( [ $class: 'JacocoPublisher' ] )
     }
     catch(Exception e) {
         //TODO - To be removed on the new version of plugin 2.2.1 of jacoco, verson 2.2.0 had issues with gitlba plugin
     }
     
  }
  /*
  * This method compile the gradle
  */
  def buildGradle() {
     // Run the maven build
     if (steps.isUnix()) {
        steps.sh "'${gradleHome}/bin/gradle' clean build "
     } else {
        steps.bat(/"${gradleHome}\bin\gradle" clean build /)
     }
     steps.step( [ $class: 'JacocoPublisher' ] )
  }

  /*
  * Se NodeJS paths to enviroment
  */
  void withNodeEnv(Closure body) {
     // Run the NodeJS build
     steps.withEnv(["NODEJS_HOME=${nodeHome}","PATH=$script.env.PATH:${nodeHome}/bin"]){
        steps.sh "echo NODEJS_HOME: ${script.env.NODEJS_HOME}"
        body.call()
    }
  }
  
  /*
  * This method compile the gradle
  */
  def buildNodeJs() {
     // Run the nodeJS build
     steps.withEnv(["NODEJS_HOME=${nodeHome}","PATH=$script.env.PATH:${nodeHome}/bin"]){
         steps.sh "echo NODEJS_HOME: ${script.env.NODEJS_HOME}"
         if (steps.isUnix()) {
            steps.sh "'${nodeHome}/bin/node' --version"
            steps.sh "'${nodeHome}/bin/npm' -v"
            steps.sh "'${nodeHome}/bin/npm' config set loglevel warn"
            steps.sh "'${nodeHome}/bin/npm' config set proxy http://127.0.0.1:3128"
            steps.sh "'${nodeHome}/bin/npm' config set https-proxy http://127.0.0.1:3128"
            steps.sh "'${nodeHome}/bin/npm' config set registry http://registry.npmjs.org/"
            steps.sh "'${nodeHome}/bin/npm' config get prefix"

            steps.sh "'${nodeHome}/bin/npm' prune"
            steps.sh "rm -rf node_modules "
            steps.sh "'${nodeHome}/bin/npm' install"
         } else {
            steps.bat(/"${nodeHome}\bin\node" --version /)
            
            steps.bat(/"${nodeHome}\bin\npm" -v /)
            
            steps.bat(/"${nodeHome}\bin\npm" prune /)
            steps.bat(/"rem TODO: rm node_modules -rf" /)
            steps.bat(/"${nodeHome}\bin\npm" install --verbose/)
         }
     }
     steps.step( [ $class: 'JacocoPublisher' ] )
  }
  /*
  * This method save the results for test and generate and the deploy artifact by extension
  */
  def saveResult(extension) { 
     steps.junit '**/target/surefire-reports/TEST-*.xml,**/build/test-results/TEST-*.xml'
     steps.archive '**/target/*.'+extension
  }
  /*
  * This method execute sonar
  */
  def executeSonar() {
     steps.withSonarQubeEnv {
       steps.withCredentials([[$class: 'StringBinding', credentialsId: 'jenkins-sonar-token', variable: 'jenkinsSonarTokenValue']]) {
       if (steps.isUnix()) {
         steps.sh "'${mvnHome}/bin/mvn' org.sonarsource.scanner.maven:sonar-maven-plugin:3.0.2:sonar -Dsonar.login=${script.env.jenkinsSonarTokenValue} -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.junit.reportsPath=target/surefire-reports -Dsonar.jacoco.reportPath=target/jacoco.exec" 
       } else {
         steps.bat(/"${mvnHome}\bin\mvn" org.sonarsource.scanner.maven:sonar-maven-plugin:3.0.2:sonar -Dsonar.login=${script.env.jenkinsSonarTokenValue} -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.junit.reportsPath=target\/surefire-reports -Dsonar.jacoco.reportPath=target\/jacoco.exec/)
       }          
       }
     }
  } 

  /*
  * This method execute sonar
  */
  def executeSonarWithGradle() {
     steps.withSonarQubeEnv {
       steps.withCredentials([[$class: 'StringBinding', credentialsId: 'jenkins-sonar-token', variable: 'jenkinsSonarTokenValue']]) {
           if (steps.isUnix()) {
             steps.sh "'${gradleHome}/bin/gradle' sonarqube -Dsonar.verbose=true -Dsonar.login=${script.env.jenkinsSonarTokenValue} -Dsonar.host.url=${script.env.SONAR_HOST_URL} -Dsonar.junit.reportsPath=build/test-results -Dsonar.jacoco.reportPath=build/jacoco/jacocoTest.exec"
           }
       }
     }
  }
  
  /*
  * This method deploy artifact on Nexus
  */
  def deployNexusArtifact(project) {
     if ("${branchName}"=="master"){
      repositoryId = project+"_release"
      steps.echo "Master deploy to release repository: ${repositoryId}"
     }else{
      repositoryId = project+"_snapshot"
      steps.echo "Deploy to snapshot repository: ${repositoryId}"
     }
     // Run the maven build
     if (steps.isUnix()) {
        steps.sh "'${mvnHome}/bin/mvn' deploy -DrepositoryId=${repositoryId}"
     } else {
        steps.bat(/"${mvnHome}\bin\mvn" deploy -DrepositoryId=${repositoryId}/)
     }
  }
  /*
  * This method deploy artifact on Nexus
  */
  def builSprintBootDockerFile(dockerHost,tag) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        steps.sh "cp ./target/*.jar ."
        steps.echo "Docker tag: ${tag}"
        newApp = docker.build(tag, "-f ./src/main/docker/Dockerfile .")
     }
  }
  
  /*
  * This method to push to Docker Registry
  */
  def pushDockerRegistry(dockerHost,registryHost,tag) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        steps.echo "Docker registryHost: ${registryHost}"
        steps.echo "Docker tag: ${tag}"
        steps.sh "docker tag ${tag} ${registryHost}/${tag}"
        steps.sh "docker push ${registryHost}/${tag}"
     }
  }

  /*
  * This method run the build on docker
  */
  def runDocker(dockerHost,containerName,exposedPorts) {
     runDocker(dockerHost,containerName,exposedPorts,"${newApp.id}")
  }

  /*
  * This method run the build on docker
  */
  def runDocker(dockerHost,containerName,exposedPorts,image) {
     steps.withEnv(["DOCKER_HOST="+dockerHost]){
        def containerId = steps.sh (
          script: 'docker ps -q --filter "name='+containerName+'"',
          returnStdout: true
        ).trim()
        steps.echo "containerId: "+containerId
        if(containerId!=null&&!containerId.isEmpty()) { 
            steps.sh (
              script: 'docker stop '+containerId,
              returnStdout: false
            )
        }
        containerId = steps.sh (
          script: 'docker ps -a -q --filter "name='+containerName+'"',
          returnStdout: true
        ).trim()
        if(containerId!=null&&!containerId.isEmpty()) { 
            steps.sh (
              script: 'docker rm '+containerId,
              returnStdout: false
            )
        }
        steps.sh "docker run -d --name="+containerName+" "+exposedPorts+" "+image
        steps.sh "docker ps"
     }
  }
  
  /*
  * This method will perform common post execution task
  */
  def executePostExecutionTasks() {
    //Clean up workspace when job was executed ok, this improve performance on server
    steps.step([$class: 'WsCleanup', cleanWhenFailure: false])
    steps.gitlabCommitStatus("success:Build#${script.env.BUILD_NUMBER}") {
       steps.sh 'echo "CI OK"'
    }
  }

  /*
  * This method will perform common post execution task
  */
  def executeOnErrorExecutionTasks() {
    //Clean up workspace when job was executed ok, this improve performance on server
     steps.updateGitlabCommitStatus(name:"failed:Build#${script.env.BUILD_NUMBER}",state: 'failed')
  }
  
  /*
  * This method notify by mail the status of Job
  */
  def notifyByMail(String status = 'STARTED',String recipients) {
    // status of null means successful
    status =  status ?: 'SUCCESSFUL'

    // Default values
    def subject = "${status}: Job '${script.env.JOB_NAME} [${script.env.BUILD_NUMBER}]'"
    def summary = "${subject} (${script.env.BUILD_URL})"
    def details = """<p>${status}: Job '${script.env.JOB_NAME} [${script.env.BUILD_NUMBER}]':</p>
      <p>Check console output at &QUOT;<a href='${script.env.BUILD_URL}'>${script.env.JOB_NAME} [${script.env.BUILD_NUMBER}]</a>&QUOT;</p>"""

    if(recipients!=null&&recipients.empty){
      // Send notifications to specific recipients
      steps.emailext (
          subject: subject,
          body: details,
          to: recipients
      )
    }else{
      if(type=='ligthweight'){
          // Send notifications to specific recipients
          steps.emailext (
              subject: subject,
              body: details,
              to: "${script.env.gitlabUserEmail}"
          )
      }else{
          // Send notifications to launch job person
          steps.emailext (
              subject: subject,
              body: details,
              recipientProviders: [[$class: 'DevelopersRecipientProvider']]
          )
      }
    }
  }
}