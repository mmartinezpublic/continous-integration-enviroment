echo SONAR_USER: ${SONAR_USER}
echo SONAR_PASSWORD: ${SONAR_PASSWORD}
echo SONAR_URL: ${SONAR_URL}
curl -u ${SONAR_USER}:${SONAR_PASSWORD} -k ${SONAR_URL}/api/user_tokens/search?login=${SONAR_USER} | jq '.[]'

TOKEN_NAME=jenkins-sonar-token
curl -u ${SONAR_USER}:${SONAR_PASSWORD} -k ${SONAR_URL}/api/user_tokens/revoke?name=${TOKEN_NAME} -X POST | jq '.[]'
curl -u ${SONAR_USER}:${SONAR_PASSWORD} -k ${SONAR_URL}/api/user_tokens/generate?name=${TOKEN_NAME} -X POST | jq '.[]'