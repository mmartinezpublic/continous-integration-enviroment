#!/bin/bash
TIME=$(date '+%d/%m/%Y %H:%M:%S');

# /vagrant_data/installation/scripts/restart-cntlm.sh
echo [${TIME}] ------------ RESTART CNTLM ------------
ERROR_ON_CURL=$(curl -Lv www.google.com 2>&1 | grep 'Failed connect to 127.0.0.1:3128; Connection refused')
if [ -z "${ERROR_ON_CURL}" ] ; then
	echo "ERROR_ON_CURL: "${ERROR_ON_CURL}
	sudo cntlm -c /etc/cntlm.conf
	CNTLM_ENABLED=$(sudo netstat -ntpa | grep 3128)
	echo CNTLM_ENABLED: ${CNTLM_ENABLED}
fi

sudo netstat -ntpa | grep 3128 | awk "{print $7}"

CNTLM_ENABLED=$(sudo netstat -ntpa | grep 3128)
if [ -z "${CNTLM_ENABLED}" ] ; then
	echo "CNTLM_ENABLED: DOWN"
	sudo cntlm -c /etc/cntlm.conf
	CNTLM_ENABLED=$(sudo netstat -ntpa | grep 3128)
	echo CNTLM_ENABLED: ${CNTLM_ENABLED}
fi
