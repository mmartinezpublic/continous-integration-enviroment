#-------------------------------------------------------------------------
# CONFIGURATION TO BE DONE ON JENKINS SETTINGS: ENVIROMENT VARIABLES
export JENKINS_HOME=/opt/jenkins
export GITLAB_PROTOCOL=http
export GITLAB_HOST=127.0.0.1
export GITLAB_PORT=8084
export USER_GITLAB=%1
export PASS_GITLAB=%2
#-------------------------------------------------------------------------

export JENKINS_HOME=${JENKINS_HOME}
export JENKINS_URL=${JENKINS_URL}
export GITLAB_PROTOCOL=${GITLAB_PROTOCOL}
export GITLAB_HOST=${GITLAB_HOST}
export GITLAB_PORT=${GITLAB_PORT}
echo JENKINS_HOME: ${JENKINS_HOME}
echo GITLAB_PROTOCOL: ${GITLAB_PROTOCOL}
echo GITLAB_HOST: ${GITLAB_HOST}
echo GITLAB_PORT: ${GITLAB_PORT}
cd ${JENKINS_HOME}

export USER_GITLAB=${USER_GITLAB}
export PASS_GITLAB=${PASS_GITLAB}
echo USER_GITLAB: ${USER_GITLAB}
echo PASS_GITLAB: ${PASS_GITLAB}

export GITLAB_PROJECT=demo-java
export PROJECT_CREATION_PARAMETERS=\&public\=false\&visibility_level\=10
export GITLAB_NAMESPACE=bmdl

SCRIPT_PATH=$(echo ${JENKINS_HOME}/tools/scripts)
echo SCRIPT_PATH: ${SCRIPT_PATH}

ls -all ${SCRIPT_PATH}
${SCRIPT_PATH}/create-gitlab-project-1.0.sh