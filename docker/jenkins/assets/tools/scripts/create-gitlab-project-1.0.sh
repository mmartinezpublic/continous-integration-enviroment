JENKINS_HOME=${JENKINS_HOME}
JENKINS_URL=${JENKINS_URL}
TEST_HOME=/tmp/ci/jenkins/test
PROJECT_FOLDER_PATH=/test/resources

echo JENKINS_HOME: ${JENKINS_HOME}
GITLAB_SERVER=${GITLAB_PROTOCOL}://${GITLAB_HOST}:${GITLAB_PORT}

echo GITLAB_SERVER: ${GITLAB_SERVER}
echo USER_GITLAB:${USER_GITLAB}
echo PASS_GITLAB:${PASS_GITLAB}
echo GITLAB_NAMESPACE: ${GITLAB_NAMESPACE}
echo PROJECT_CREATION_PARAMETERS:${PROJECT_CREATION_PARAMETERS}
cd ${JENKINS_HOME}${PROJECT_FOLDER_PATH}/${GITLAB_PROJECT}
if [ ! "$(pwd)" = "${JENKINS_HOME}${PROJECT_FOLDER_PATH}/${GITLAB_PROJECT}" ]; then
	echo Project doesnt exits on path: ${JENKINS_HOME}${PROJECT_FOLDER_PATH}/${GITLAB_PROJECT}
	exit 1
fi	

echo CLEANING TEMP FOLDER FROM : ${TEST_HOME}
if [ -d "${TEST_HOME}" ]; then
  rm -rf ${TEST_HOME}
fi

mkdir -p ${TEST_HOME}/${GITLAB_PROJECT}
cp -r ${JENKINS_HOME}${PROJECT_FOLDER_PATH}/${GITLAB_PROJECT} ${TEST_HOME}
chmod -R 777 ${TEST_HOME}
cd ${TEST_HOME}/${GITLAB_PROJECT}
echo TEMP_FOLDER: $(pwd)

echo GET PRIVATE KEY
PRIVATE_TOKEN=$(curl -s ${GITLAB_SERVER}/api/v3/session/ --data-urlencode login=${USER_GITLAB} --data-urlencode password=${PASS_GITLAB} | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^private_token/ {print $2}')
PRIVATE_TOKEN=$(echo "${PRIVATE_TOKEN}" | sed -r 's/["]+//g')
echo ${PRIVATE_TOKEN}

if [ -z ${GITLAB_NAMESPACE} ]
then
	echo "Error obtaining private key"
	exit 1  
fi

#INSTALL JQ JSON PROCESSOR
#curl -L -o jq https://github.com/stedolan/jq/releases/download/jq-1.5/jq-linux64
#chmod +x ./jq
#cp jq /usr/bin

if [ ! -z ${GITLAB_NAMESPACE} ]
then
	NAMESPACE_ID=$(curl -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X GET "${GITLAB_SERVER}/api/v3/namespaces?search=${GITLAB_NAMESPACE}" | jq '.[] | select(.kind=="group") | select(.path=="'${GITLAB_NAMESPACE}'").id' )
	echo NAMESPACE_ID: ${NAMESPACE_ID}
	if [ -z ${NAMESPACE_ID} ] 
	then
		echo "Error: Namespace not founded: ${GITLAB_NAMESPACE}"
		exit 1  
	fi
	NAMESPACE_ID=\&namespace_id=${NAMESPACE_ID}
fi

PROJECT_NAME=$(curl -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X GET "${GITLAB_SERVER}/api/v3/projects/owned" | jq '.[] | select(.name=="'${GITLAB_PROJECT}'").name')
PROJECT_NAME=$(echo ${PROJECT_NAME} | sed 's/"//g')
echo PROJECT_NAME: ${PROJECT_NAME}

if [ "${PROJECT_NAME}" = "${GITLAB_PROJECT}" ]; then
	echo DELETING... ${PROJECT_NAME}
	PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X GET "${GITLAB_SERVER}/api/v3/projects/owned" | jq '.[] | select(.name=="'${GITLAB_PROJECT}'").id')
	DELETE_MESSAGE=$(curl -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X DELETE "${GITLAB_SERVER}/api/v3/projects/${PROJECT_ID}")
	echo ${DELETE_MESSAGE}
	echo DELETED ${PROJECT_NAME}
fi

echo CREATE REPOSITORY
echo \"curl -s --header PRIVATE-TOKEN:${PRIVATE_TOKEN} -X POST \"${GITLAB_SERVER}/api/v3/projects?name=${GITLAB_PROJECT}${NAMESPACE_ID}${PROJECT_CREATION_PARAMETERS}\"
CREATE_PROJECT_LOG=$(curl -s --header PRIVATE-TOKEN:${PRIVATE_TOKEN} -X POST "${GITLAB_SERVER}/api/v3/projects?name=${GITLAB_PROJECT}${NAMESPACE_ID}${PROJECT_CREATION_PARAMETERS}")
echo ${CREATE_PROJECT_LOG}

#ERROR BY DELAY ON DELETION
CREATE_PROJECT_LOG1=$(echo ${CREATE_PROJECT_LOG} | grep "The project is still being deleted. Please try again later.")
#ERROR BY DELAY ON CREATION
CREATE_PROJECT_LOG2=$(echo ${CREATE_PROJECT_LOG} | grep "has already been taken")
CREATE_PROJECT_LOG=${CREATE_PROJECT_LOG1}${CREATE_PROJECT_LOG2}
echo ${CREATE_PROJECT_LOG}

if [ ! -z "${CREATE_PROJECT_LOG}" ]
then
   sleep 3
   echo CREATE REPOSITORY - RETRY 2
   CREATE_PROJECT_LOG=$(curl -s --header PRIVATE-TOKEN:${PRIVATE_TOKEN} -X POST "${GITLAB_SERVER}/api/v3/projects?name=${GITLAB_PROJECT}${NAMESPACE_ID}${PROJECT_CREATION_PARAMETERS}")
   echo CREATE_PROJECT_LOG: ${CREATE_PROJECT_LOG}
fi

PROJECT_ID=$(curl -s --header "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" -X GET "${GITLAB_SERVER}/api/v3/projects/owned" | jq '.[] | select(.name=="'${GITLAB_PROJECT}'").id')
echo PROJECT_ID:${PROJECT_ID}

if [ -z ${PROJECT_ID} ]
then
	echo "Error creating the project"
	exit 1  
fi

if [ ! -z "${PROJECT_ID}" ]
then
	if [ ! -z "${JENKINS_PROJECT_WEBHOOK}" ]
	then
		WEBHOOK_URL=${JENKINS_PROJECT_WEBHOOK}
		WEBHOOK_PARAMETERS=\&push_events=true\&merge_requests_events=true
		echo FULL_WEBHOOK_URL:${GITLAB_SERVER}/api/v3/projects/${PROJECT_ID}/hooks?url=${WEBHOOK_URL}${WEBHOOK_PARAMETERS}
		CREATE_WEBHOOK_RESULT=$(curl -s --header PRIVATE-TOKEN:${PRIVATE_TOKEN} -X POST "${GITLAB_SERVER}/api/v3/projects/${PROJECT_ID}/hooks?url=${WEBHOOK_URL}${WEBHOOK_PARAMETERS}")
		echo CREATE_WEBHOOK_RESULT:${CREATE_WEBHOOK_RESULT}
	fi
fi

echo home: $HOME

git config --global user.name ${USER_GITLAB}
git config --global user.email ${USER_GITLAB}@test.com
git config credential.helper store

echo current folder $(pwd)
ls -all

git init

GIT_PATH=${GITLAB_PROTOCOL}://${USER_GITLAB}:${PASS_GITLAB}@${GITLAB_HOST}:${GITLAB_PORT}/${GITLAB_NAMESPACE}/${GITLAB_PROJECT}.git
echo ${GIT_PATH}

REMOTE_SET=$(git remote -v | grep ${GITLAB_PROJECT})
if [ ! -z "${REMOTE_SET}" ]
then
   git remote remove ${GITLAB_PROJECT}
fi

git remote add ${GITLAB_PROJECT} ${GIT_PATH}
git remote -v
git add .
git commit -m "commit ${GITLAB_PROJECT}"

git push -u ${GITLAB_PROJECT} master

if [ -d "${TEST_HOME}" ]; then
  rm -rf ${TEST_HOME}
fi