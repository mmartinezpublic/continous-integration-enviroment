#!/bin/sh
set -e
STATUS=0

function check_service() 
{ 
	wget -q --spider $2
	status=$?
	if [ $status -eq 0 ]; then
	     echo "$1 OK"
	else
		 echo "$1 KO"
		 STATUS=1
	fi
}

function check_ports_availability() 
{ 
	netstat -lnt | (grep -r ":$2" & grep -r "LISTEN" & grep "0.0.0.0:*") >/dev/null 2>&1 &
	status=$?
	if [ $status -eq 0 ]; then
		echo "$1 OK"
	else
		echo "$1 KO"
		STATUS=1
	fi
}

echo 'Service status:'
SERVICE=JENKINS
SERVICE_URL=http://prmcclnxd01:8080/login?from=%2F
check_service ${SERVICE} ${SERVICE_URL}

SERVICE=GITLAB
SERVICE_URL=http://127.0.0.1:8084/users/sign_in
check_service ${SERVICE} ${SERVICE_URL}

SERVICE=NEXUS
SERVICE_URL=http://prmcclnxd01:8081/nexus
check_service ${SERVICE} ${SERVICE_URL}

SERVICE=SONAR
SERVICE_URL=http://prmcclnxd02:8082/sonarqube/
check_service ${SERVICE} ${SERVICE_URL}

SERVICE=GITLAB_DATABASE
SERVICE_PORT=3306
check_ports_availability ${SERVICE} ${SERVICE_PORT}

SERVICE=SSH
SERVICE_PORT=22
check_ports_availability ${SERVICE} ${SERVICE_PORT}

SERVICE=DOCKER_REGISTRY
SERVICE_PORT=5000
check_ports_availability ${SERVICE} ${SERVICE_PORT}

echo ''
echo 'Filesystem'
df -BM | grep -vE '^Filesystem|tmpfs|cdrom' | awk '{ print $5 " " $1 " " $2 " " $3 " " $4 " " $6 }'

echo ''
echo 'Backups - Filesystem'
du --block-size=1M --max-depth=1 -a /opt/backup/backups | sort -n -r | head -n 20

echo ''
echo 'Memory'
echo 'total  used  free'
free -m | awk 'NR==2 {print $2 " " $3 " " $4}'

if [ ! "${STATUS}" = "0" ]; then
	echo "ENVIROMENT ERROR DETECTED!"
	exit 1
fi