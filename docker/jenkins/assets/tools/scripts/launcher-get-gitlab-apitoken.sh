#-------------------------------------------------------------------------
# CONFIGURATION TO BE DONE ON JENKINS SETTINGS: ENVIROMENT VARIABLES
export JENKINS_HOME=/opt/jenkins
#-------------------------------------------------------------------------

JENKINS_HOME=${JENKINS_HOME}
echo JENKINS_HOME: ${JENKINS_HOME}
cd ${JENKINS_HOME}

export USER_GITLAB=%1
export PASS_GITLAB=%2
export GITLAB_PROTOCOL=%3
export GITLAB_HOST=%4
export GITLAB_PORT=%5

SCRIPT_PATH=$(echo ${JENKINS_HOME}/tools/scripts)
echo SCRIPT_PATH: ${SCRIPT_PATH}

ls -all ${SCRIPT_PATH}
${SCRIPT_PATH}/get-gitlab-apitoken-1.0.sh