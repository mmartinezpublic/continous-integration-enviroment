GITLAB_SERVER=${GITLAB_PROTOCOL}://${GITLAB_HOST}:${GITLAB_PORT}
echo GITLAB_SERVER: ${GITLAB_SERVER}

echo GET PRIVATE KEY
PRIVATE_TOKEN=$(curl -s ${GITLAB_SERVER}/api/v3/session/ --data-urlencode login=${USER_GITLAB} --data-urlencode password=${PASS_GITLAB} | sed -e 's/[{}]/''/g' | awk -v RS=',"' -F: '/^private_token/ {print $2}')
PRIVATE_TOKEN=$(echo "${PRIVATE_TOKEN}" | sed -r 's/["]+//g')
echo PRIVATE_TOKEN: ${PRIVATE_TOKEN}