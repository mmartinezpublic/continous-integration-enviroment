set CERTIFICATION_PATH=/opt/jenkins/tools/certificate
set JAVA_SDK_PATH=/usr/java/jdk1.8.0_74
set STORE_PASSWORD=changeit

cd ${CERTIFICATION_PATH}/app
BALANCEADOR_URL=BalanceadorAD.credito.bcp.com.pe:636

echo INSTALL NEW CERTIFICATION
java -jar ./installcert-usn-20140115.jar ${BALANCEADOR_URL}

#Install chain of certification if needed
#sudo cp ./certificados/bcp-enterprise-certification-authority-sha256.cer ${CERTIFICATION_PATH}/
#sudo cp ./certificados/credicorp-root-certification-authority.cer ${CERTIFICATION_PATH}/

#REMOVE OLD CERTIFICATE
#sudo keytool -delete -noprompt -alias 'bcp-enterprise-certification-authority-sha256' -keystore ${JAVA_SDK_PATH}/jre/lib/security/cacerts -storepass ${STORE_PASSWORD}
#sudo keytool -delete -noprompt -alias 'credicorp-root-certification-authority' -keystore ${JAVA_SDK_PATH}/jre/lib/security/cacerts -storepass ${STORE_PASSWORD}

#INSTALL CERTIFICATE
sudo keytool -import -alias bcp-enterprise-certification-authority-sha256 -file bcp-enterprise-certification-authority-sha256.cer -keystore ${JAVA_SDK_PATH}/jre/lib/security/cacerts -storepass ${STORE_PASSWORD}
sudo keytool -import -alias credicorp-root-certification-authority -file credicorp-root-certification-authority.cer -keystore ${JAVA_SDK_PATH}/jre/lib/security/cacerts -storepass ${STORE_PASSWORD} 

#sudo service nexus restart
#sudo gitlab-ctl restart
#sudo service sonarqube restart