import hudson.*
import hudson.security.*
import jenkins.model.*
import java.util.*
import com.michelin.cio.hudson.plugins.rolestrategy.*
import java.lang.reflect.*

// Check if enabled
def env = System.getenv()
def funtionality=env['ROLE_BASED_AUTH_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> ROLE_BASED_AUTH Disabled"
    return
}

/**
 * ============   Roles   ============
 */
def globalRoleAnonymous = "anonymous"
def globalRoleRead = "read"
def globalBuildRole = "build"
def globalRoleAdmin = "admin"

/**
 * ========  Users and Groups ========
 */
 
env = System.getenv()
def ldapGroupNameAdmin = env['LDAP_GROUP_NAME_ADMIN'] ?: "administrators"
def admins = ["admin",ldapGroupNameAdmin]
def builders = []
def readers = []
def anonymous = ["anonymous"]

/**
 * ==========  Permissions  ==========
 */

// TODO: drive these from a config file
def adminPermissions = [
"hudson.model.View.Delete",
"hudson.model.Computer.Connect",
"hudson.model.Run.Delete",
"hudson.model.Hudson.UploadPlugins",
"com.cloudbees.plugins.credentials.CredentialsProvider.ManageDomains",
"hudson.model.Computer.Create",
"hudson.model.View.Configure",
"jenkins.metrics.api.Metrics.View",
"hudson.model.Hudson.ConfigureUpdateCenter",
"hudson.model.Computer.Build",
"hudson.model.Item.Configure",
"hudson.model.Hudson.Administer",
"hudson.model.Item.Cancel",
"hudson.model.Item.Read",
"com.cloudbees.plugins.credentials.CredentialsProvider.View",
"hudson.model.Computer.Delete",
"jenkins.metrics.api.Metrics.HealthCheck",
"hudson.model.Item.Build",
"hudson.scm.SCM.Tag",
"hudson.model.Item.Move",
"hudson.model.Item.Discover",
"hudson.model.Hudson.Read",
"com.cloudbees.plugins.credentials.CredentialsProvider.Update",
"hudson.model.Item.Create",
"hudson.model.Item.Workspace",
"com.cloudbees.plugins.credentials.CredentialsProvider.Delete",
"hudson.model.Run.Replay",
"jenkins.metrics.api.Metrics.ThreadDump",
"hudson.model.View.Read",
"hudson.model.Hudson.RunScripts",
"hudson.model.View.Create",
"hudson.model.Item.Delete",
"hudson.model.Computer.Configure",
"com.cloudbees.plugins.credentials.CredentialsProvider.Create",
"hudson.model.Computer.Disconnect",
"hudson.model.Run.Update"
]

def readPermissions = [
"hudson.model.Hudson.Read",
"hudson.model.Item.Discover",
"hudson.model.Item.Read"
]

def buildPermissions = [
"hudson.model.Hudson.Read",
"hudson.model.Item.Build",
"hudson.model.Item.Cancel",
"hudson.model.Item.Read",
"hudson.model.Run.Replay"
]

def anonymousPermissions = [
"hudson.model.Item.Discover"
]

def roleBasedAuthenticationStrategy = new RoleBasedAuthorizationStrategy()
Jenkins.instance.setAuthorizationStrategy(roleBasedAuthenticationStrategy)


/**
 * ===================================
 *         
 *               HACK
 * Inspired by https://issues.jenkins-ci.org/browse/JENKINS-23709
 * Deprecated by on https://github.com/jenkinsci/role-strategy-plugin/pull/12
 *
 * ===================================
 */

Constructor[] constrs = Role.class.getConstructors();
for (Constructor<?> c : constrs) {
  c.setAccessible(true);
}

// Make the method assignRole accessible
Method assignRoleMethod = RoleBasedAuthorizationStrategy.class.getDeclaredMethod("assignRole", String.class, Role.class, String.class);
assignRoleMethod.setAccessible(true);
println "HACK! changing visibility of RoleBasedAuthorizationStrategy.assignRole"

Thread.start {
    sleep 15000
	/**
	 * ===================================
	 *         
	 *           Permissions
	 *
	 * ===================================
	 */
	
	Set<Permission> adminPermissionSet = new HashSet<Permission>();
	adminPermissions.each { p ->
	  def permission = Permission.fromId(p);
	  if (permission != null) {
	    adminPermissionSet.add(permission);
	  } else {
	    println "${p} is not a valid permission ID (ignoring)"
	  }
	}
	
	Set<Permission> buildPermissionSet = new HashSet<Permission>();
	buildPermissions.each { p ->
	  def permission = Permission.fromId(p);
	  if (permission != null) {
	    buildPermissionSet.add(permission);
	  } else {
	    println "${p} is not a valid permission ID (ignoring)"
	  }
	}
	
	Set<Permission> readPermissionSet = new HashSet<Permission>();
	readPermissions.each { p ->
	  def permission = Permission.fromId(p);
	  if (permission != null) {
	    readPermissionSet.add(permission);
	  } else {
	    println "${p} is not a valid permission ID (ignoring)"
	  }
	}
	
	Set<Permission> anonymousPermissionSet = new HashSet<Permission>();
	anonymousPermissions.each { p ->
	  def permission = Permission.fromId(p);
	  if (permission != null) {
	    anonymousPermissionSet.add(permission);
	  } else {
	    println "${p} is not a valid permission ID (ignoring)"
	  }
	}
	
	/**
	 * ===================================
	 *         
	 *      Permissions -> Roles
	 *
	 * ===================================
	 */
	
	// admins
	Role adminRole = new Role(globalRoleAdmin, adminPermissionSet);
	roleBasedAuthenticationStrategy.addRole(RoleBasedAuthorizationStrategy.GLOBAL, adminRole);
	
	// builders
	Role buildersRole = new Role(globalRoleAdmin, buildPermissionSet);
	roleBasedAuthenticationStrategy.addRole(RoleBasedAuthorizationStrategy.GLOBAL, buildersRole);
	
	// readers
	Role readRole = new Role(globalRoleRead, readPermissionSet);
	roleBasedAuthenticationStrategy.addRole(RoleBasedAuthorizationStrategy.GLOBAL, readRole);
	
	// anonymous
	Role anonymousRole = new Role(globalRoleAnonymous, anonymousPermissionSet);
	roleBasedAuthenticationStrategy.addRole(RoleBasedAuthorizationStrategy.GLOBAL, anonymousRole);
	
	/**
	 * ===================================
	 *         
	 *      Roles -> Groups/Users
	 *
	 * ===================================
	 */
	
	admins.each { l ->
	  println "Granting admin to ${l}"
	  roleBasedAuthenticationStrategy.assignRole(RoleBasedAuthorizationStrategy.GLOBAL, adminRole, l);  
	}
	
	builders.each { l ->
	  println "Granting builder to ${l}"
	  roleBasedAuthenticationStrategy.assignRole(RoleBasedAuthorizationStrategy.GLOBAL, buildersRole, l);  
	}
	
	readers.each { l ->
	  println "Granting read to ${l}"
	  roleBasedAuthenticationStrategy.assignRole(RoleBasedAuthorizationStrategy.GLOBAL, readRole, l);  
	}	
	
	readers.each { l ->
	  println "Granting anonymous to ${l}"
	  roleBasedAuthenticationStrategy.assignRole(RoleBasedAuthorizationStrategy.GLOBAL, anonymousRole, l);  
	}
	
	Jenkins.instance.save()
}