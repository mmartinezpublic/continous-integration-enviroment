import hudson.model.*;
import jenkins.model.*;
import hudson.tools.*;
import hudson.plugins.gradle.GradleInstaller;
import hudson.plugins.gradle.GradleInstallation;
import jenkins.mvn.*;

def env = System.getenv()
def funtionality=env['GRADLE_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> GRADLE Disabled"
    return
}

def gradle_version = env['GRADLE_VERSION']
def gradle_home = env['GRADLE_HOME']

// Constants
def instance = Jenkins.getInstance()

Thread.start {
    // Gradle
    println "--> Configuring Gradle Installation"
    def desc_GradleTool = instance.getDescriptor("hudson.plugins.gradle.Gradle")
    def gradle_installations = desc_GradleTool.getInstallations()

	def version=gradle_version
	if (gradle_home==null) {
      gradle_home=""
    }
	
    //def gradleInstaller = new GradleInstaller(version)
    //def installSourceProperty = new InstallSourceProperty([gradleInstaller])
    
    def name="GRADLE_" + version

    def gradle_inst = new GradleInstallation(
      name, // Name
      gradle_home, // Home
      null //[installSourceProperty]
    )

    // Only add a Gradle installation if it does not already exist - do not overwrite existing config
    
    def gradle_inst_exists = false
    gradle_installations.each {
      installation = (GradleInstallation) it
        if ( gradle_inst.getName() ==  installation.getName() ) {
                gradle_inst_exists = true
                println("Found existing installation: " + installation.getName())
        }
    }
    
    if (!gradle_inst_exists) {
        gradle_installations += gradle_inst
    }

    desc_GradleTool.setInstallations((GradleInstallation[]) gradle_installations)
    desc_GradleTool.save()
    
    // Save the state
    instance.save()
}