import hudson.model.*;
import jenkins.model.*;

def env = System.getenv()
def gitGlobalConfigName = env['GIT_GLOBAL_CONFIG_NAME']
def gitGlobalConfigEmail = env['GIT_GLOBAL_CONFIG_EMAIL']
// Constants
def instance = Jenkins.getInstance()

Thread.start {
	println env
    println "--> Configuring GIT Settings"

    // Git Identity
    println "--> Configuring Git Identity"
    def desc_git_scm = instance.getDescriptor("hudson.plugins.git.GitSCM")
    desc_git_scm.setGlobalConfigName(gitGlobalConfigName)
    desc_git_scm.setGlobalConfigEmail(gitGlobalConfigEmail)

    // Save the state
    instance.save()
}
