import jenkins.model.*
import com.cloudbees.plugins.credentials.*
import com.cloudbees.plugins.credentials.common.*
import com.cloudbees.plugins.credentials.domains.*
import com.cloudbees.plugins.credentials.impl.*
import com.cloudbees.jenkins.plugins.sshcredentials.impl.*
import com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl
import org.jenkinsci.plugins.plaincredentials.impl.*
import hudson.util.Secret
import hudson.plugins.sshslaves.*;

Thread.start {
	def store = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
	
	def domain = Domain.global()
	def apiTokenId = "jenkins-gitlab-apitoken"
	def apiToken = "DEMO"
	
	//Create a ssh credential for slave
	def priveteKey = new BasicSSHUserPrivateKey(
	CredentialsScope.SYSTEM,
	"jenkins-slave-key",
	"root",
	new BasicSSHUserPrivateKey.UsersPrivateKeySource(),
	"",
	""
	)
	
	//Create a basic credential for slave
	def usernameAndPassword = new UsernamePasswordCredentialsImpl(
	  CredentialsScope.SYSTEM,
	  "jenkins-slave-credentials", "Jenkis Slave with Password Configuration",
	  "root",
	  "jenkins"
	)
	
	//Create a GITLAB credential
	def jenkinsGitlabApiToken = new GitLabApiTokenImpl(
	  CredentialsScope.SYSTEM,
	  "jenkins-gitlab-apitoken", "Jenkis Gitlab API Token",
	  new Secret(apiToken)
	)
	
	//Create a GITLAB sharedlib credentials
	usernameAndPassword = new UsernamePasswordCredentialsImpl(
	  CredentialsScope.SYSTEM,
	  "gitlab-sharedlib-credentials", "Jenkis Gitlab sharelib credentials",
	  "xxxx",
	  "xxxx"
	)
	
	//Create a GITLAB test user credentials
	usernameAndPassword = new UsernamePasswordCredentialsImpl(
	  CredentialsScope.SYSTEM,
	  "gitlab-test-project-credentials", "Jenkis Gitlab test project credentials",
	  "xxxx",
	  "xxxx"
	)
	
	//Create a SONAR_API credential
	def jenkinsSonarToken = secretText = new StringCredentialsImpl(
	CredentialsScope.GLOBAL,
	"jenkins-sonar-token",
	"jenkins-sonar-token",
	Secret.fromString("some secret text goes here")
	)
	
	store.addCredentials(domain, priveteKey)
	store.addCredentials(domain, usernameAndPassword)
	store.addCredentials(domain, jenkinsGitlabApiToken)
	store.addCredentials(domain, jenkinsSonarToken)
}