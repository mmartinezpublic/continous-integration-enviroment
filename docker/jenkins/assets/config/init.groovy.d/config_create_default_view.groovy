import hudson.model.*;
import jenkins.model.*;

def env = System.getenv()
// Constants
def instance = Jenkins.getInstance()

Thread.start {
	def viewName='TEMPLATES'
	view=instance.getView(viewName)
	
	if(view==null){
	   def newListViewObj = new ListView(viewName)   
	   instance.addView(newListViewObj)
	}
		
	viewName='TEST-ENVIROMENT'
	view=instance.getView(viewName)
	
	if(view==null){
	   def newListViewObj = new ListView(viewName)   
	   instance.addView(newListViewObj)
	}
	// Save the state
	instance.save()
}