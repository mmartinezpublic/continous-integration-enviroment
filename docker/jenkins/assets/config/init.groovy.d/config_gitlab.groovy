import hudson.model.*;
import jenkins.model.*;
import com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl;

def env = System.getenv()
if (env['GITLAB_ENABLED']!= null && !env['GITLAB_ENABLED'].toBoolean()) {
    println "--> GITLAB Disabled"
    return
}

def gitLabName = env['GITLAB_CONNECTION_NAME']
def gitLabUrl = env['GITLAB_CONNECTION_URL']
def apiTokenId = "jenkins-gitlab-apitoken"
def ignoreCertificateErrors=true
def connectionTimeout=10
def readTimeout=10
// Constants
def instance = Jenkins.getInstance()

//Credential APITOKEN for jenkins
def JENKINS_GITLAB_APITOKEN="jenkins-gitlab-apitoken"
def store = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getStore()
def jenkinsGitlabApiToken=null

Thread.start {
    // Configuring GitLab Connection
    println "--> Configuring GitLab Connection"
    def descGitLabConnectionConfig = instance.getDescriptor("com.dabsquared.gitlabjenkins.connection.GitLabConnectionConfig")
    def connections= descGitLabConnectionConfig.getConnections()
    connections.clear()
    
    List credentialsList = Jenkins.instance.getExtensionList('com.cloudbees.plugins.credentials.SystemCredentialsProvider')[0].getCredentials()
    credentialsList.each {
    	def credentials=it
        def className=credentials.getClass().getName()
        if(className.equals("com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl")){
          println("Child: "+className)
          GitLabApiTokenImpl apiTokenCredentials=credentials
          String descrition=apiTokenCredentials.getDescription()
          String tempId=apiTokenCredentials.getId()
          println("tempId: "+tempId)
          if(JENKINS_GITLAB_APITOKEN.equals(tempId)){
            jenkinsGitlabApiToken=tempId
          	println("apiTokenId: "+apiTokenId)
          	return
          }
        }
    }
    
    def gitLabConnection=new com.dabsquared.gitlabjenkins.connection.GitLabConnection(gitLabName, gitLabUrl, jenkinsGitlabApiToken, ignoreCertificateErrors, connectionTimeout, readTimeout)
    descGitLabConnectionConfig.addConnection(gitLabConnection)
	descGitLabConnectionConfig.save()

    // Save the state
    instance.save()
}    