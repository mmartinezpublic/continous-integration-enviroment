import hudson.model.*;
import jenkins.model.*;
import com.dabsquared.gitlabjenkins.connection.GitLabApiTokenImpl;

def env = System.getenv()
if (env['DOCKER_ENABLED']== null || !env['DOCKER_ENABLED'].toBoolean()) {
    println "--> DOCKER Disabled"
    return
}

def docker_host = env['DOCKER_HOST']
println "docker_host --> "+docker_host

def instance = Jenkins.getInstance()

Thread.start {
    globalNodeProperties = instance.getGlobalNodeProperties()
    envVarsNodePropertyList = globalNodeProperties.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class)
    
    newEnvVarsNodeProperty = null
    envVars = null
    
    if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
      newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty();
      globalNodeProperties.add(newEnvVarsNodeProperty)
      envVars = newEnvVarsNodeProperty.getEnvVars()
    } else {
      envVars = envVarsNodePropertyList.get(0).getEnvVars()
    
    }
    
    envVars.put("DOCKER_HOST", docker_host)

    // Save the state
    instance.save()
}    