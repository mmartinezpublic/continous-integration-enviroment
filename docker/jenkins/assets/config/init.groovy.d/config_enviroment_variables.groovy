import jenkins.model.*
instance = Jenkins.getInstance()

Thread.start {
	globalNodeProperties = instance.getGlobalNodeProperties()
	envVarsNodePropertyList = globalNodeProperties.getAll(hudson.slaves.EnvironmentVariablesNodeProperty.class)
	
	newEnvVarsNodeProperty = null
	envVars = null
	
	if ( envVarsNodePropertyList == null || envVarsNodePropertyList.size() == 0 ) {
	  newEnvVarsNodeProperty = new hudson.slaves.EnvironmentVariablesNodeProperty();
	  globalNodeProperties.add(newEnvVarsNodeProperty)
	  envVars = newEnvVarsNodeProperty.getEnvVars()
	} else {
	  envVars = envVarsNodePropertyList.get(0).getEnvVars()
	
	}
	
	envVars.put("GITLAB_PROTOCOL", "http")
	envVars.put("GITLAB_HOST", "gitlab")
	envVars.put("GITLAB_PORT", "80")
	
	instance.save()
}