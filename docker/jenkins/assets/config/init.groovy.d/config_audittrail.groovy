import jenkins.model.*;
import hudson.plugins.audit_trail.AuditTrailPlugin
import hudson.plugins.audit_trail.LogFileAuditLogger
import java.util.logging.Logger

def env = System.getenv()
if (env['AUDITTRAIL_ENABLED']== null || !env['AUDITTRAIL_ENABLED'].toBoolean()) {
    println "--> AUDITTRAIL Disabled"
    return
}

def audit_trail_log_path = env['AUDITTRAIL_LOG_PATH']
def audit_trail_log_file_size = env['AUDITTRAIL_LOG_FILE_SIZE'].toInteger()
def audit_trail_log_file_count = env['AUDITTRAIL_LOG_FILE_COUNT'].toInteger()
println "audit_trail_log_path --> "+audit_trail_log_path
println "audit_trail_log_file_size --> "+audit_trail_log_file_size
println "audit_trail_log_file_count --> "+audit_trail_log_file_count

def instance = Jenkins.getInstance()

Thread.start {
    println "--> Configuring Audit Trail"
	LogFileAuditLogger logAuditLogger = new LogFileAuditLogger(
		audit_trail_log_path, 
		audit_trail_log_file_size, 
		audit_trail_log_file_count)
	
	AuditTrailPlugin plugin = instance.getPlugin(AuditTrailPlugin.class);
	plugin.loggers.clear()
	plugin.loggers.add(logAuditLogger)
	plugin.save()
	plugin.start()
	
}    