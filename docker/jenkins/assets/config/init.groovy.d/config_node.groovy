import jenkins.model.*
import hudson.model.*
import jenkins.plugins.nodejs.tools.*
import hudson.tools.*

def env = System.getenv()
def funtionality=env['NODEJS_ENABLED']
if (funtionality== null || !funtionality.toBoolean()) {
    println "--> NODEJS Disabled"
    return
}

def nodejs_version = env['NODE_VERSION']
def nodejs_home = env['NODE_HOME']
println "nodejs_version --> "+nodejs_version
println "nodejs_home --> "+nodejs_home

// Constants
def instance = Jenkins.getInstance()

Thread.start {
    println "--> Configuring NODEJS Settings"
	
	def installations = [];
	def desc = instance.getDescriptor("jenkins.plugins.nodejs.tools.NodeJSInstallation")
	def installation = new NodeJSInstallation("NODEJS_"+nodejs_version, nodejs_home,null)
	installations.push(installation)

	desc.setInstallations(installations.toArray(new NodeJSInstallation[0]))

	instance.save()
}
