import hudson.model.*;
import jenkins.model.*;

// Variables
def env = System.getenv()
def dockerName = "Docker"
def dockerHome = env['DOCKER_HOME']
def apiTokenId = ""
def ignoreCertificateErrors=true
def connectionTimeout=10
def readTimeout=10
// Constants
def instance = Jenkins.getInstance()

Thread.start {
    // Configuring Docker Tool Connection
    println "--> Configuring Docker Tool Connection"
    def descDockerTool = instance.getDescriptor("org.jenkinsci.plugins.docker.commons.tools.DockerTool")    
    def dockerTool=new org.jenkinsci.plugins.docker.commons.tools.DockerTool(dockerName, dockerHome, null)
    descDockerTool.setInstallations(dockerTool)
	descDockerTool.save()

    // Save the state
    instance.save()
}    