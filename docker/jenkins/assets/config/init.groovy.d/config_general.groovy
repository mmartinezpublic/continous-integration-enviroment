import hudson.model.*;
import jenkins.model.*;

def env = System.getenv()
def funtionality=env['GENERAL_SETTINGS_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> GENERAL_SETTINGS Disabled"
    return
}

def root_Url = env['ROOT_URL']
def masterNumExecutors = env['JENKINS_MASTER_NUM_EXECUTORS'].toInteger()

// Constants
def instance = Jenkins.getInstance()

Thread.start {
    println "--> Configuring General Settings"
	
	// Configure master executors
	instance.setNumExecutors(masterNumExecutors)

    // Base URL
    println "--> Setting Base URL"
    jlc = JenkinsLocationConfiguration.get()
    jlc.setUrl(root_Url)
    jlc.save()

    // Save the state
    instance.save()
}
