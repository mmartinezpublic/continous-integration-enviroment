import hudson.model.*;
import jenkins.model.*;
import hudson.tools.*;
import hudson.tasks.Maven.MavenInstaller;
import hudson.tasks.Maven.MavenInstallation;
import jenkins.mvn.*;

def env = System.getenv()
def funtionality=env['MAVEN_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> MAVEN Disabled"
    return
}

def maven_version = env['MAVEN_VERSION']
def maven_home = env['MAVEN_HOME']
def maven_conf_path = env['MAVEN_CONF_PATH']

// Constants
def instance = Jenkins.getInstance()

Thread.start {   
    // Maven
    println "--> Configuring Maven Installation"
    def desc_MavenTool = instance.getDescriptor("hudson.tasks.Maven")
    def maven_installations = desc_MavenTool.getInstallations()

	def version=maven_version
	if (maven_home==null) {
       maven_home=""
    }
	
    //def mavenInstaller = new MavenInstaller(version)
    //def installSourceProperty = new InstallSourceProperty([mavenInstaller])

    def name="MAVEN_" + version

    def maven_inst = new MavenInstallation(
      name, // Name
      maven_home, // Home
      null //[installSourceProperty]
    )

    // Only add a Maven installation if it does not already exist - do not overwrite existing config
    
    def maven_inst_exists = false
    maven_installations.each {
      installation = (MavenInstallation) it
        if ( maven_inst.getName() ==  installation.getName() ) {
                maven_inst_exists = true
                println("Found existing installation: " + installation.getName())
        }
    }
    
    if (!maven_inst_exists) {
        maven_installations += maven_inst
    }

    desc_MavenTool.setInstallations((MavenInstallation[]) maven_installations)
    desc_MavenTool.save()
    
    println "--> Configuring Global Maven Config"
	// Configure Global Maven Config
	def desc_GlobalMavenConfig = Jenkins.instance.getExtensionList(
	  jenkins.mvn.GlobalMavenConfig.class
	)[0]
	
	def desc_SettingsProvider = new jenkins.mvn.FilePathGlobalSettingsProvider(maven_conf_path+"/settings.xml")
	desc_GlobalMavenConfig.setGlobalSettingsProvider(desc_SettingsProvider)
	desc_GlobalMavenConfig.save()

    // Save the state
    instance.save()
}