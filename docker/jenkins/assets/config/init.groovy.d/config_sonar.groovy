import hudson.model.*
import jenkins.model.*
import hudson.plugins.sonar.*
import hudson.plugins.sonar.model.TriggersConfig
import hudson.plugins.sonar.utils.SQServerVersions
import hudson.tools.*

def env = System.getenv()
def funtionality=env['SONAR_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> ADOP SonarQube Disabled"
    return
}

// Variables
def sonar_version=SQServerVersions.SQ_5_3_OR_HIGHER;
def sonar_server_url = env['SONAR_SERVER_URL']
def sonar_account_login = env['SONAR_ACCOUNT_LOGIN']
def sonar_account_password = env['SONAR_ACCOUNT_PASSWORD']
def sonar_db_url = env['SONAR_DB_URL']
def sonar_db_login = env['SONAR_DB_LOGIN']
def sonar_db_password = env['SONAR_DB_PASSWORD']
def sonar_plugin_version = env['SONAR_PLUGIN_VERSION']
def sonar_additional_props = env['SONAR_ADDITIONAL_PROPS']
def sonar_runner_version = env['SONAR_RUNNER_VERSION']

// Constants
def instance = Jenkins.getInstance()

Thread.start {
    // Sonar
    // Source: http://pghalliday.com/jenkins/groovy/sonar/chef/configuration/management/2014/09/21/some-useful-jenkins-groovy-scripts.html
    println "--> Configuring SonarQube"
    def SonarGlobalConfiguration sonar_conf = instance.getDescriptor(SonarGlobalConfiguration.class)

    def sonar_inst = new SonarInstallation(
        "SONARQUBE_"+sonar_version, // Name
        sonar_server_url,
        sonar_version, // Major version upgrade of server would require to change it
        "", // Token
        sonar_db_url,
        sonar_db_login,
        sonar_db_password,
        sonar_plugin_version,
        sonar_additional_props,
        new TriggersConfig(),
        sonar_account_login,
        sonar_account_password,
        "" // Additional Analysis Properties
    )

    // Only add ADOP Sonar if it does not exist - do not overwrite existing config
    def sonar_installations = sonar_conf.getInstallations()
    def sonar_inst_exists = false
    sonar_installations.each {
        installation = (SonarInstallation) it
        if (sonar_inst.getName() == installation.getName()) {
            sonar_inst_exists = true
            println("Found existing installation: " + installation.getName())
        }
    }

    if (!sonar_inst_exists) {
        sonar_installations += sonar_inst
        sonar_conf.setInstallations((SonarInstallation[]) sonar_installations)
        sonar_conf.save()
    }

    // Save the state
    instance.save()
}