import hudson.model.*;
import jenkins.model.*;

def env = System.getenv()
def funtionality=env['JDK_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> JDK Disabled"
    return
}

// Constants
def instance = Jenkins.getInstance()

Thread.start {
    println "--> Configuring JDK Settings"
	
	sdkName="OPEN_JDK8"
	sdkFolderPath="/usr/lib/jvm/java-8-openjdk-amd64"
	def sdkFolder = new File( sdkFolderPath )

	def jdkDescriptor = instance.getDescriptor(JDK)
	
	// If it doesn't exist
	if( sdkFolder.exists() ) {
		jdkDescriptor.setInstallations(
	        new JDK(sdkName, sdkFolderPath)
		)
	}
	
	instance.save()
}
