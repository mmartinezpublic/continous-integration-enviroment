import hudson.model.*;
import jenkins.model.*;
import hudson.tools.*;
import hudson.tasks.Maven.MavenInstaller;
import hudson.tasks.Maven.MavenInstallation;
import jenkins.mvn.*;
import org.jenkinsci.plugins.workflow.libs.SCMSourceRetriever;
import org.jenkinsci.plugins.workflow.libs.LibraryConfiguration;
import jenkins.plugins.git.GitSCMSource;

def env = System.getenv()
def funtionality=env['PIPELINE_SHARELIB_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> PIPELINE_SHARELIB Disabled"
    return
}

def sharedlib_git_url = env['SHAREDLIB_GIT_URL']

Thread.start {  
	def globalLibsDesc = Jenkins.getInstance()
	          .getDescriptor("org.jenkinsci.plugins.workflow.libs.GlobalLibraries")
	SCMSourceRetriever retriever = new SCMSourceRetriever(new GitSCMSource(
	        "jenkins-sharedlib",
	        sharedlib_git_url,
	        "",
	        "",
	        "",
	        false))
	LibraryConfiguration pipeline = new LibraryConfiguration("jenkins-sharedlib", retriever)
	globalLibsDesc.get().setLibraries([pipeline])
}