import hudson.model.*;
import jenkins.model.*;

def env = System.getenv()
def funtionality=env['MAIL_ENABLED']
if (funtionality!= null && !funtionality.toBoolean()) {
    println "--> MAIL Disabled"
    return
}

// Variables
def stmpUser = env['SMTP_USER']
def stmpPassword = env['SMTP_PASSWORD']
def stmpHost = env['SMTP_HOST']
def stmpPort = env['SMTP_PORT']
def stmpUseSSL = env['SMTP_USE_SSL'].toBoolean()
def stmpCharset = env['SMTP_CHARSET']
def stmpReplyMail = env['SMTP_REPLY_MAIL']
def stmpAdminMail = env['SMTP_ADMIN_MAIL']
def stmpAdminName = env['SMTP_ADMIN_NAME']

// Constants
def instance = Jenkins.getInstance()

Thread.start {
	println env
    println "--> Configuring Mail Settings"
 
	//Set the mail server configuration
	def desc = instance.getDescriptor("hudson.tasks.Mailer")
	desc.setSmtpAuth(stmpUser, stmpPassword)
	desc.setReplyToAddress(stmpReplyMail)
	desc.setSmtpHost(stmpHost)
	desc.setUseSsl(stmpUseSSL)
	desc.setSmtpPort(stmpPort)
	desc.setCharset(stmpCharset)
	desc.save()
	
	//Set the administrator email address
	def jenkinsLocationConfiguration = JenkinsLocationConfiguration.get()
	jenkinsLocationConfiguration.setAdminAddress("["+stmpAdminName+"] <"+stmpAdminMail+">")
	jenkinsLocationConfiguration.save()
	
    // Save the state
    instance.save()
}

