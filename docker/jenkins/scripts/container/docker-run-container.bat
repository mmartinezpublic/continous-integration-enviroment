@ECHO OFF
cls

ECHO. [%time%] - EXECUTE RUN DOCKER CONTAINER
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..
SET CURRENT_DIR=%CD%\
SET PORTS="-p 8081:8081 -p 50000:50000"

FOR /F "tokens=*" %%G  IN (%CURRENT_DIR%config\process-name.txt) DO (
  SET PROCESS_NAME=%%G
)

FOR /F "tokens=*" %%G  IN (%CURRENT_DIR%config\image-name.txt) DO (
  ECHO.DOCKER RUN %%G
  docker run --cidfile "%CURRENT_DIR%cid" -d --name %PROCESS_NAME% %PORTS% %%G
  ECHO.START - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE