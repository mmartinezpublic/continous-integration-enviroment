@ECHO OFF
cls

ECHO. [%time%] - EXECUTE DOCKER COMMAND: BACKUP
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

FOR /F "tokens=*" %%G  IN (%CURRENT_DIR%cid) DO (
  ECHO.BKP %%G
  docker exec -i -t %%G /var/sonar_bkp/scripts/backup-launcher.sh
  ECHO.START - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE