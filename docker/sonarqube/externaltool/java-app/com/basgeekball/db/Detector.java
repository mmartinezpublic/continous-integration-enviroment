package com.basgeekball.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Detector {
  public static void main(String[] args) throws InterruptedException {
    try {
      Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {
      System.out.println("Can not find JDBC driver for MySQL.");
      e.printStackTrace();
      System.exit(1);
    }
    String url = System.getProperty("SONARQUBE_JDBC_URL");
    String user =  System.getProperty("SONARQUBE_JDBC_USERNAME");
    String password = System.getProperty("SONARQUBE_JDBC_PASSWORD");
    int retries =  System.getProperty("RETRY_TIME");
    long interval =  System.getProperty("RETRY_INTERVAL");
    Connection connection;
    for (int i = 0; i < retries; i++) {
      try {
        connection = DriverManager.getConnection(url, user, password);
        if (connection != null) {
          System.out.println("DB connection is successful.");
          return;
        }
      } catch (SQLException e) {
        System.out.println("Can not establish a connection to the DB.");
      }
      Thread.sleep(interval);
    }
    System.out.println("Failed to connect to the DB.  Quit.");
    System.exit(1);
  }
}
