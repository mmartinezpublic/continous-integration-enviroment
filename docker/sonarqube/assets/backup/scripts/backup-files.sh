#!/bin/bash
set -e
##################################################################################
function usage(){
  echo "usage: $(basename $0) /path/to/bkp_home archive.tar.gz"
}
##################################################################################

readonly APP_HOME=$1
readonly DEST_FILE=$2
readonly CUR_DIR=$(cd $(dirname ${BASH_SOURCE:-$0}); pwd)
readonly TMP_DIR="$CUR_DIR/tmp"
readonly ARC_NAME="app-backup"
readonly ARC_DIR="$TMP_DIR/$ARC_NAME"
readonly TMP_TAR_NAME="$TMP_DIR/archive.tar.gz"

if [ -z "$APP_HOME" -o -z "$DEST_FILE" ] ; then
  usage >&2
  exit 1
fi

rm -rf "$ARC_DIR" "$TMP_TAR_NAME"

echo backup $APP_HOME/conf/
mkdir -p $ARC_DIR/conf

if [ "$(ls -A $APP_HOME/conf/)" ]; then
  cp -R "$APP_HOME/conf/"* "$ARC_DIR/conf"
fi

echo backup $APP_HOME/extensions/plugins/
mkdir -p $ARC_DIR/extensions/plugins

if [ "$(ls -A $APP_HOME/extensions/plugins/)" ]; then
  cp -R "$APP_HOME/extensions/plugins/"* "$ARC_DIR/extensions/plugins"
fi

if [ ! -d "${TMP_DIR}" ]; then
 echo making dir: ${TMP_DIR}
 mkdir ${TMP_DIR}
fi

cd $ARC_DIR
pwd
tar -czvf "$TMP_TAR_NAME" conf extensions
cd -
mkdir -p `dirname $DEST_FILE`  
mv -f "$TMP_TAR_NAME" "$DEST_FILE"
rm -rf "$ARC_DIR"

MAX_BACKUPS=${MAX_BACKUPS}
echo "=> Backup started: ${BACKUP_NAME}"
if [ -n "${MAX_BACKUPS}" ]; then
    while [ $(ls ${BKP_FILES_FOLDER} -N1 | wc -l) -gt ${MAX_BACKUPS} ];
    do
        BACKUP_TO_BE_DELETED=$(ls ${BKP_FILES_FOLDER} -N1 | sort | head -n 1)
        echo "   Backup ${BACKUP_TO_BE_DELETED} is deleted"
        rm -rf ${BKP_FILES_FOLDER}/${BACKUP_TO_BE_DELETED}
    done
fi
echo "=> Backup done"

exit 0