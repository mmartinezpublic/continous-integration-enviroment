#!/bin/bash

if [ "${MYSQL_ENV_MYSQL_PASS}" == "**Random**" ]; then
        unset MYSQL_ENV_MYSQL_PASS
fi

MYSQL_HOST=${MYSQL_PORT_3306_TCP_ADDR:-${MYSQL_HOST}}
MYSQL_HOST=${MYSQL_PORT_1_3306_TCP_ADDR:-${MYSQL_HOST}}
MYSQL_PORT=${MYSQL_PORT_3306_TCP_PORT:-${MYSQL_PORT}}
MYSQL_PORT=${MYSQL_PORT_1_3306_TCP_PORT:-${MYSQL_PORT}}
MYSQL_USER=${MYSQL_USER:-${MYSQL_ENV_MYSQL_USER}}
MYSQL_PASS=${MYSQL_PASS:-${MYSQL_ENV_MYSQL_PASS}}
BKP_FILES_FOLDER=${BKP_FILES_FOLDER}

if [ ! -d "${BKP_FILES_FOLDER}" ]; then
 echo making dir: ${BKP_FILES_FOLDER}
 mkdir ${BKP_FILES_FOLDER}
fi

[ -z "${MYSQL_HOST}" ] && { echo "=> MYSQL_HOST cannot be empty" && exit 1; }
[ -z "${MYSQL_PORT}" ] && { echo "=> MYSQL_PORT cannot be empty" && exit 1; }
[ -z "${MYSQL_USER}" ] && { echo "=> MYSQL_USER cannot be empty" && exit 1; }
[ -z "${MYSQL_PASS}" ] && { echo "=> MYSQL_PASS cannot be empty" && exit 1; }

BACKUP_NAME=$(echo db-${MYSQL_DB}-$(date +%Y%m%d.%H%M%S).sql)
BACKUP_CMD="mysqldump -h${MYSQL_HOST} -P${MYSQL_PORT} -u${MYSQL_USER} -p${MYSQL_PASS} ${EXTRA_OPTS} ${MYSQL_DB} --result-file=${BKP_FILES_FOLDER}/${BACKUP_NAME}"

MAX_BACKUPS=${MAX_BACKUPS}
echo "=> Backup started: ${BACKUP_NAME}"
if ${BACKUP_CMD} ;then	
	cd "${BKP_FILES_FOLDER}"
	echo tar -czvf "${BKP_FILES_FOLDER}"/"${BACKUP_NAME}".tar.gz "${BACKUP_NAME}"
    tar -czvf "${BKP_FILES_FOLDER}"/"${BACKUP_NAME}".tar.gz "${BACKUP_NAME}"
    cd -
	rm -rf "${BKP_FILES_FOLDER}"/"${BACKUP_NAME}"
    echo "   Backup succeeded"
else
    echo "   Backup failed"
    rm -rf ${BKP_FILES_FOLDER}/${BACKUP_NAME}
fi
if [ -n "${MAX_BACKUPS}" ]; then
    while [ $(ls ${BKP_FILES_FOLDER} -N1 | wc -l) -gt ${MAX_BACKUPS} ];
    do
        BACKUP_TO_BE_DELETED=$(ls ${BKP_FILES_FOLDER} -N1 | sort | head -n 1)
        echo "   Backup ${BACKUP_TO_BE_DELETED} is deleted"
        rm -rf ${BKP_FILES_FOLDER}/${BACKUP_TO_BE_DELETED}
    done
fi
echo "=> Backup done"