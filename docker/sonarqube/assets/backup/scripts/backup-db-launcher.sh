#!/bin/bash

export APP_NAME=sonar
export MYSQL_HOST=prmcclnxd02
export MYSQL_PORT=3306
export MYSQL_USER=sonaruser
export MYSQL_PASS=sonar123
export MYSQL_DB=sonarqube53
export MAX_BACKUPS=10
export BKP_FILES_FOLDER=${BKP_FOLDER}/backups/${APP_NAME}/db

echo BKP_FILES_FOLDER: ${BKP_FILES_FOLDER}

if [ ! -d "${BKP_FILES_FOLDER}" ]; then
 echo making dir: ${BKP_FILES_FOLDER}
 sudo mkdir -p ${BKP_FILES_FOLDER}
fi

${BKP_FOLDER}/${APP_NAME}-bkp/scripts/backup-db.sh