#!/bin/bash
APP_NAME=sonar
APP_HOME=/opt/sonar/sonarqube-5.3
BKP_FILES_FOLDER=${BKP_FOLDER}/backups/${APP_NAME}/files

echo BKP_FILES_FOLDER: ${BKP_FILES_FOLDER}

if [ ! -d "${BKP_FILES_FOLDER}" ]; then
 echo making dir: ${BKP_FILES_FOLDER}
 mkdir -p ${BKP_FILES_FOLDER}
fi

${BKP_FOLDER}/${APP_NAME}-bkp/scripts/backup-files.sh ${APP_HOME} ${BKP_FILES_FOLDER}/${APP_NAME}-bkp-$(date +%Y%m%d.%H%M%S).tar.gz
