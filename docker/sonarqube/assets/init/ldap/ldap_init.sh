#!/bin/bash
set -e

LDAP_USER_DN=${LDAP_ADMIN_USER_DN}
LDAP_PASSWORD=${LDAP_ADMIN_PASSWORD}
LDAP_BASE_DN=${LDAP_BASE_DN}
LDAP_LDIFF="/tmp/structure.ldif.temp"
HOSTNAME=$(cat /etc/hostname)
TMP_ENVS="/tmp/env_tmp"
APPLICATION_INIT_FOLDER=${APPLICATION_INIT_FOLDER}

# resolve environment variables in ldif file
cp ${LDAP_LDIFF} "${LDAP_LDIFF}.tmp"
env | sed 's/[\%]/\\&/g;s/\([^=]*\)=\(.*\)/s%${\1}%\2%/' > ${TMP_ENVS}
cat "${LDAP_LDIFF}" | sed -f ${TMP_ENVS} > "${LDAP_LDIFF}.tmp"
rm ${TMP_ENVS}
cp "${LDAP_LDIFF}.tmp" /var/sonar_bkp/backups
ls /var/sonar_bkp/backups

echo -e "Start polling for LDAP autoconfiguration [HOSTNAME: ${LDAP_HOSTNAME}, LDAP_USER_DN ${LDAP_USER_DN}, LDAP_BASE_DN: ${LDAP_BASE_DN}, LDAP_LDIFF: ${LDAP_LDIFF}.tmp]"
nohup ${APPLICATION_INIT_FOLDER}/ldap/load_ldif.sh -h ${LDAP_HOSTNAME} -u ${LDAP_USER_DN} -p ${LDAP_PASSWORD} -b ${LDAP_BASE_DN} -f ${LDAP_LDIFF}.tmp &

exit 0
