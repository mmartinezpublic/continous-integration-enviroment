#!/bin/bash
set -e
trap "break;exit" SIGHUP SIGINT SIGTERM

APPLICATION_INIT_FOLDER=/opt/sonarqube/init
DOCKER_ENTRYPOINT=/opt/sonarqube/bin/run.sh

echo "SETUP LDAP"
if [ ! -f ${APPLICATION_INIT_FOLDER}/INITIAL_DATA_DONE ]; then
	echo "INITIAL DATA"
   	${APPLICATION_INIT_FOLDER}/ldap/ldap_init.sh
   	echo "SUCCESS - INITIAL DATA"
   	echo "SUCCESS - INITIAL DATA" >> ${APPLICATION_INIT_FOLDER}/INITIAL_DATA_DONE
fi

echo "STARTING APPLICATION:"${APPLICATION_INIT_FOLDER}
${APPLICATION_INIT_FOLDER}/wait-for-it.sh --host=${SONARQUBE_DB_HOST} --port=${SONARQUBE_DB_PORT} --timeout=80

if [ "$LDAP_ENABLED" = true ]
  then
  SONAR_ARGUMENTS+=" -Dsonar.security.realm=LDAP \
    -Dsonar.security.savePassword=false \
    -Dldap.url=ldap://${LDAP_HOSTNAME} \
    -Dldap.bindDn=${LDAP_ADMIN_USER_DN} \
    -Dldap.bindPassword=${LDAP_ADMIN_PASSWORD} \
    -Dldap.user.realNameAttribute=${LDAP_DISPLAY_NAME_ATTRIBUTE_NAME} \
    -Dldap.user.emailAttribute=${LDAP_MAIL_ADDRESS_ATTRIBUTE_NAME} \
    -Dldap.group.idAttribute=${LDAP_GROUP_ID_ATTRIBUTE} \
	-Dldap.user.baseDn=${LDAP_USER_SEARCH_BASE} \
    -Dldap.user.request=${LDAP_USER_SEARCH} \
    -Dldap.group.baseDn=${LDAP_GROUP_SEARCH_BASE} \
    -Dldap.group.request=${LDAP_GROUP_SEARCH_FILTER}"	
fi

if [ "$HTTP_PROXY_ENABLED" = true ]
  then  
  SONAR_ARGUMENTS+=" -Dhttp.proxyhost=${HTTP_PROXY_HOST} \
    -Dhttp.proxyport=${HTTP_PROXY_PORT}"	
fi

echo ${DOCKER_ENTRYPOINT} ${SONAR_ARGUMENTS}
${DOCKER_ENTRYPOINT} ${SONAR_ARGUMENTS}