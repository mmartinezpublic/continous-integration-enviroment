#!/bin/bash
set -e
trap "break;exit" SIGHUP SIGINT SIGTERM

APPLICATION_INIT_FOLDER=/opt/sonarqube/init
DOCKER_ENTRYPOINT=/opt/sonarqube/bin/run.sh

echo "STARTIN APPLICATION"
if [ ! -f ${APPLICATION_INIT_FOLDER}/INITIAL_DATA_DONE ]; then
	echo "INITIAL DATA"
   	${APPLICATION_INIT_FOLDER}/ldap/ldap_init.sh
   	echo "SUCCESS - INITIAL DATA"
   	echo "SUCCESS - INITIAL DATA" >> ${APPLICATION_INIT_FOLDER}/INITIAL_DATA_DONE
fi

echo "SUCCESS" >> ${REDMINE_SETUP_FOLDER}/DONE
${APPLICATION_INIT_FOLDER}/wait-for-it.sh --host=db --port=3306 --timeout=80
${DOCKER_ENTRYPOINT}