@ECHO OFF
cls

ECHO. [%time%] - EXECUTE DOCKER COMPOSE CLEAN
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..

rmdir /s /q %CD%\volumen\nexus
mkdir %CD%\volumen\nexus

rmdir /s /q %CD%\volumen\mysql
mkdir %CD%\volumen\mysql

rmdir /s /q %CD%\volumen\jenkins
mkdir %CD%\volumen\jenkins
mkdir %CD%\volumen\jenkins\home
mkdir %CD%\volumen\jenkins\backups

rmdir /s /q %CD%\volumen\sonar
mkdir %CD%\volumen\sonar
mkdir %CD%\volumen\sonar\backups
mkdir %CD%\volumen\sonar\data

rmdir /s /q %CD%\volumen\redis
mkdir %CD%\volumen\redis

rmdir /s /q %CD%\volumen\git
mkdir %CD%\volumen\git
mkdir %CD%\volumen\git\data

PAUSE