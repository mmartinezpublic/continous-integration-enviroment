#!/bin/bash
set -e

clear

TIME=$(date '+%d/%m/%Y %H:%M:%S');

echo [${TIME}] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CURRENT_DIR=$(pwd)
echo [${TIME}] - CURRENT DIRECTORY: ${CURRENT_DIR};

cd ..

kompose --verbose -f docker-compose.yml up
