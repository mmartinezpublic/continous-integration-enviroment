@ECHO OFF
cls

ECHO. [%time%] - EXECUTE DOCKER COMMAND: STATS
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

SET IMAGE_NAME=continousintegration_nexus
SET COMMAND="docker ps -q --filter=ancestor=%IMAGE_NAME%"

FOR /F "tokens=*" %%F IN ('%COMMAND%') DO (
  ECHO.docker stats %%F
  docker stats %%F
  ECHO.START - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE