@ECHO OFF
cls

ECHO. [%time%] - EXECUTE DOCKER COMMAND: BACKUP
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

SET IMAGE_NAME=continousintegration_jenkins
SET COMMAND="docker ps -q --filter=ancestor=%IMAGE_NAME%"

ECHO.%PATH%
FOR /F "tokens=*" %%F IN ('%COMMAND%') DO (
 docker exec -i -t %%F /var/jenkins_bkp/scripts/backup-launcher.sh
)

PAUSE