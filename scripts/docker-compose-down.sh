#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/continous-integration-enviroment/scripts/docker-compose-down.sh

cd ..
CURRENT_DIR=$(pwd)
echo [$(${TIME})] - EXECUTE DOCKER COMPOSE DOWN
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

read -rsp $'Press enter to continue...\n'
rm -f docker-compose-down.log
docker-compose stop | tee -a docker-compose-down.log

