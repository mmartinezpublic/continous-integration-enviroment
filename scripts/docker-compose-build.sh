#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/continous-integration-enviroment/scripts/docker-compose-build.sh

cd ..
CURRENT_DIR=$(pwd)
echo [$(${TIME})] - EXECUTE DOCKER COMPOSE BUILD
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

read -rsp $'Press enter to continue...\n'

rm -f docker-compose-build.log
docker-compose build | tee -a docker-compose-build.log