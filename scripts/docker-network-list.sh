#!/bin/bash
set -e
TIME=date\ +%d/%m/%Y-%H:%M:%S
CURRENT_DIR=$(dirname "$0")
cd ${CURRENT_DIR}
clear

# /docker_projects/continous-integration-enviroment/scripts/docker-network-create.sh

echo [$(${TIME})] - EXECUTE DOCKER COMPOSE UP
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
echo [$(${TIME})] - CURRENT DIRECTORY: ${CURRENT_DIR};

docker network list
