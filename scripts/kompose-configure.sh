#!/bin/bash
set -e

clear

TIME=$(date '+%d/%m/%Y %H:%M:%S');

echo [${TIME}] - EXECUTE CONFIGURE KUBERNETS
echo DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CURRENT_DIR=$(pwd)
echo [${TIME}] - CURRENT DIRECTORY: ${CURRENT_DIR};

cd ..

CLUSTER=example-cluster
SERVER=http://localhost:8001
tail -n1000 ~/.kube/config
kubectl config set-cluster ${CLUSTER} --server=${SERVER}
kubectl config set-context default-system --cluster=${CLUSTER}
kubectl config use-context default-system
kubectl get pods --server=${SERVER}
