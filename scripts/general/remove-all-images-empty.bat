@ECHO OFF
cls

ECHO. [%time%] - EXECUTE STOP DOCKER CONTAINERS
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..

FOR /F "tokens=1,2 delims=:" %%G  IN ('docker images --filter "dangling=true" -q --no-trunc') DO (
  ECHO.STOPPING docker CID %%G: %%H
  docker rmi %%H
  ECHO.STOPPED - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE