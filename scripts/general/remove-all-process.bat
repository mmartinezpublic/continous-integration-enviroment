@ECHO OFF
cls

ECHO. [%time%] - EXECUTE REMOVE DOCKER PROCESS
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..

FOR /F "tokens=1,2 delims=," %%G  IN ('docker ps -a --format "{{.Names}},{{.ID}}"') DO (
  ECHO.REMOVING docker CID %%G: %%H
  docker rm %%H
  ECHO.REMOVE - ERRORLEVEL: %ERRORLEVEL%
)

DEL /Q CID

PAUSE