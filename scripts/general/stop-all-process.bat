@ECHO OFF
cls

ECHO. [%time%] - EXECUTE STOP DOCKER CONTAINERS
ECHO.DONE by: MIGUEL ANGEL MARTINEZ ESPICHAN
CD %~dp0
SET CURRENT_DIR=%~dp0
ECHO. [%time%] - CURRENT DIRECTORY: %CURRENT_DIR%
%~d0

CD /D %CURRENT_DIR%
CD..

FOR /F "tokens=1,2 delims=," %%G  IN ('docker ps -a --format "{{.Names}},{{.ID}}"') DO (
  ECHO.STOPPING docker CID %%G: %%H
  docker stop %%H
  ECHO.STOPPED - ERRORLEVEL: %ERRORLEVEL%
)

PAUSE